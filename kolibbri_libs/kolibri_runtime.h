#ifndef KOLIBRI_RUNTIME_H
#define KOLIBRI_RUNTIME_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <strings.h>
#include <string.h>

typedef struct _string _kolibri_string;
_kolibri_string create_kolibri_string(const char*);
void resize_kolibri_string(_kolibri_string*, uint32_t);
void append_int_kolibri_string(_kolibri_string* str, int);
void append_char_kolibri_string(_kolibri_string*, char);
void append__kolibri_string_kolibri_string(_kolibri_string*, const _kolibri_string*);
_kolibri_string _kolibri_string_plus_int(const _kolibri_string*, int);
_kolibri_string _kolibri_string_plus_char(const _kolibri_string*, char);
_kolibri_string _kolibri_string_plus__kolibri_string(const _kolibri_string*, const _kolibri_string*);
_kolibri_string int_to_s(int i);

void puts_kolibri(const _kolibri_string*);

typedef struct _range _kolibri_range;

typedef struct _string {
  char* str;
  uint32_t size;
  uint32_t capacity;
} _kolibri_string;

typedef  struct _range {
    int32_t start;
    int32_t end;
    int32_t step;
} _kolibri_range;

_kolibri_string create_kolibri_string(const char* str) {
  uint32_t str_len = (uint32_t) strlen(str);
  _kolibri_string new_str;

  new_str.size = str_len;
  new_str.capacity = (str_len + 1) * 2;
  new_str.str = malloc(sizeof(char) * new_str.capacity);

  strncpy(new_str.str, str, str_len);


  return new_str;
}

void resize_kolibri_string(_kolibri_string* str, uint32_t additional_size) {
  while ((str->size + 1) + additional_size > str->capacity) {
      str->capacity *= 2;
      str->str = realloc(str->str, sizeof(char) * str->capacity);
    }
}

void append_int_kolibri_string(_kolibri_string* str, int i) {
  char int_buff[22];
  int j = sprintf(int_buff, "%d", i);

  resize_kolibri_string(str, (uint32_t) j);

  for(uint32_t x = 0; x <= (uint32_t) j; x++) {
      str->str[str->size + x] = int_buff[x];
    }

  str->size += (uint32_t) j;
  str->str[str->size] = '\0';
}

void append_char_kolibri_string(_kolibri_string* str, char c) {
  resize_kolibri_string(str, 1);

  str->str[str->size] = c;

  str->size += 1;
  str->str[str->size] = '\0';
}

void append__kolibri_string_kolibri_string(_kolibri_string* str, const _kolibri_string* append_str) {
  resize_kolibri_string(str, append_str->size);

  strcat(str->str, append_str->str);

  str->size += append_str->size;
  str->str[str->size] = '\0';
}

_kolibri_string _kolibri_string_plus_int(const _kolibri_string* left_side, int right_side) {
    _kolibri_string new_str = create_kolibri_string(left_side->str);
    append_int_kolibri_string(&new_str, right_side);
    return new_str;
}

_kolibri_string _kolibri_string_plus_char(const _kolibri_string* left_side, char right_side) {
    _kolibri_string new_str = create_kolibri_string(left_side->str);
    append_char_kolibri_string(&new_str, right_side);
    return new_str;
}

_kolibri_string _kolibri_string_plus__kolibri_string(const _kolibri_string* left_side,
                                                     const _kolibri_string* right_side) {
    _kolibri_string new_str = create_kolibri_string(left_side->str);
    append__kolibri_string_kolibri_string(&new_str, right_side);
    return new_str;
}

_kolibri_string int_to_s(int i) {
    char int_buff[22];
    int j = sprintf(int_buff, "%d", i);
    return create_kolibri_string(int_buff);
}

void puts_kolibri(const _kolibri_string* value) {
  printf("%s\n", value->str);
}

#endif // KOLIBRI_RUNTIME_H
