#pragma once

#include "token.hpp"

#include <vector>
#include <utility>
#include <string>

namespace Kolibri {

class AstNode {
public:
    virtual void accept(class Visitor&) = 0;
    virtual ~AstNode() {};
};

class Expr : public AstNode {
public:
    Expr(Token token, std::string filepath);
    Expr(Token token, std::string filepath, std::string type);
    virtual ~Expr();
    Token get_token();
    const std::string& get_filepath();
    void set_type(std::string type);
    const std::string& get_type();
    bool is_mutable() { return m_is_mutable; }
    void set_mutable() { m_is_mutable = true; }
private:
    bool m_is_mutable = false;
    std::string m_filepath;
    Token m_token;
    std::string m_type = "void";
};

class Stmt : public AstNode {
public:
    Stmt(Token token, std::string filepath) : m_token(token), m_filepath(filepath) {}
    virtual ~Stmt() {};
    Token get_token() { return m_token; }
    const std::string& get_filepath() { return m_filepath; }
private:
    std::string m_filepath;
    Token m_token;
};

class BlockStmt : public AstNode {
public:
    BlockStmt(std::string filepath) : m_filepath(filepath) {}
    void add_child(AstNode* child) { m_children.push_back(child); }
    const std::vector<AstNode*>& get_children() const { return m_children; }
    void accept(Visitor&);
    std::vector<AstNode*> get_children() { return m_children; }
    const std::string& get_filepath() { return m_filepath; }
private:
    std::string m_filepath;
    std::vector<AstNode*> m_children;
};

class ElseStmt : public Stmt {
public:
    ElseStmt(Token token, std::string filepath) : Stmt(token, filepath) {}
    void add_child(Stmt* child) {
        m_children.push_back(child);
    }
    void accept(Visitor&);
    std::vector<Stmt*> get_children() { return m_children; }
private:
    std::vector<Stmt*> m_children;
};

class IfStmt : public Stmt {
public:
    IfStmt(Token token, Expr* condition, std::string filepath) : Stmt(token, filepath), m_condition(condition)
    {}
    void add_child(Stmt* child) {
        m_children.push_back(child);
    }
    void accept(Visitor&);
    Expr* get_condition() { return m_condition; };
    std::vector<Stmt*> get_children() { return m_children; }
    void add_else_if(IfStmt* else_if) { m_else_ifs.push_back(else_if); }
    void set_else_stmt(ElseStmt* else_stmt) { m_else_stmt = else_stmt; }
    std::vector<IfStmt*> get_else_ifs() { return m_else_ifs; }
    ElseStmt* get_else_stmt() const { return m_else_stmt; }
private:
    Expr* m_condition;
    ElseStmt* m_else_stmt = nullptr;
    std::vector<Stmt*> m_children;
    std::vector<IfStmt*> m_else_ifs;
};

class UnlessStmt : public Stmt {
public:
    UnlessStmt(Token token, Expr* condition, std::string filepath) : Stmt(token, filepath), m_condition(condition)
    {}
    void accept(Visitor&);
    void add_child(Stmt* child) {
        m_children.push_back(child);
    }
    std::vector<Stmt*> get_children() { return m_children; }
    Expr* get_condition() { return m_condition; };
    void set_else_stmt(ElseStmt* else_stmt) { m_else_stmt = else_stmt; }
    ElseStmt* get_else_stmt() const { return m_else_stmt; }
private:
    Expr* m_condition;
    ElseStmt* m_else_stmt;
    std::vector<Stmt*> m_children;
};

class ExprStmt : public Stmt {
public:
    ExprStmt(Expr* expr, std::string filepath) : Stmt(expr->get_token(), filepath), m_expr(expr) {}
    void accept(Visitor&);
    Expr* get_expr() { return m_expr; }
private:
    Expr* m_expr;
};

class ForStmt : public Stmt {
public:
    ForStmt(Token token, Expr* expr, std::string filepath)
        : Stmt(token, filepath), m_condition(expr) {}
    void accept(Visitor&);
    void add_child(Stmt* stmt) { m_children.push_back(stmt); }
    std::vector<Stmt*> get_children() { return m_children; }
    Expr* get_condition() { return m_condition; }
private:
    Expr* m_condition;
    std::vector<Stmt*> m_children;
};

class IntLiteral : public Expr {
public:
    IntLiteral(Token token, std::string filepath) : Expr(token, filepath, "int") {}
    void accept(Visitor&);
private:
};

class StringLiteral : public Expr {
public:
    StringLiteral(Token token, std::string filepath) : Expr(token, filepath, "string") {}
    void accept(Visitor&);
private:
};

class ListLiteral : public Expr {
public:
    ListLiteral(Token token, std::string filepath) : Expr(token, filepath, "list") {}
    void accept(Visitor&);
    void add_item(Expr* expr) { m_items.push_back(expr); }
    std::vector<Expr*> get_items() { return m_items; }
    void set_item_type(std::string type) { m_item_type = type; }
    std::string get_item_type() { return m_item_type; }
private:
    std::vector<Expr*> m_items;
    std::string m_item_type;
};

class MemberExpr : public Expr {
public:
    MemberExpr(Token token, std::string filepath, Expr* container, Expr* member)
        : Expr(token, filepath), m_container(container), m_member(member) {};
    Expr* get_container() const { return m_container; }
    Expr* get_member() const { return m_member; }
    void accept(Visitor&);
private:
    Expr* m_container;
    Expr* m_member;
};

class CharLiteral : public Expr {
public:
    CharLiteral(Token token, std::string filepath) : Expr(token, filepath, "char") {}
    void accept(Visitor&);
private:
};

class TrueLiteral : public Expr {
public:
    TrueLiteral(Token token, std::string filepath) : Expr(token, filepath, "bool") {}
    void accept(Visitor&);
private:
};

class FalseLiteral : public Expr {
public:
    FalseLiteral(Token token, std::string filepath) : Expr(token, filepath, "bool") {}
    void accept(Visitor&);
private:
};

class IdentifierLiteral : public Expr {
public:
    IdentifierLiteral(Token token, std::string filepath) : Expr(token, filepath) {}
    void accept(Visitor&);
    const std::string get_name() { return this->get_token().get_lexeme(); }
private:
};

class StructDecl : public Stmt {
public:
    StructDecl(Token identifier, std::string filepath) : Stmt(identifier, filepath) {}
    void accept(Visitor&);
    void add_member(Token type, Token name) {
        m_member.push_back({type, name});
    }
    std::vector<std::pair<Token, Token>> get_members() {
        return m_member;
    }
    std::string get_name() { return get_token().get_lexeme(); }
private:
    std::vector<std::pair<Token, Token>> m_member;
};

class StructInstance : public Expr {
public:
    StructInstance(Token identifier, std::string filepath) : Expr(identifier, filepath) {}
    void accept(Visitor&);
    void add_member(Token type, Expr* expr) {
        m_member.push_back({type, expr});
    }
    std::vector<std::pair<Token, Expr*>> get_members() {
        return m_member;
    }
    std::string get_name() { return get_token().get_lexeme(); }
private:
    std::vector<std::pair<Token, Expr*>> m_member;
};

class BinaryExpr : public Expr {
public:
    BinaryExpr(Token op, Expr* left, Expr* right, std::string filepath)
        : Expr(op, filepath), m_left_expr(left), m_right_expr(right) {}
    void accept(Visitor&);
    Expr* get_left_expr() { return m_left_expr; }
    Expr* get_right_expr() { return m_right_expr; }
private:
    Expr* m_left_expr;
    Expr* m_right_expr;
};

class UnaryExpr : public Expr {
public:
    UnaryExpr(Token op, Expr* expr, std::string filepath)
        : Expr(op, filepath), m_expr(expr) {}
    void accept(Visitor&);
    Expr* get_expr() { return m_expr; }
private:
    Expr* m_expr;
};

class ReturnStmt : public Stmt {
public:
    ReturnStmt(Token token, Expr* expr, std::string filepath)
        : Stmt(token, filepath), m_expr(expr) {}
    Expr* get_expr() { return m_expr; }
    void accept(Visitor&);
private:
    Expr* m_expr;
};

class FunctionCall : public Expr {
public:
    FunctionCall(Token token, std::string filepath) : Expr(token, filepath) {}
    void add_param(Expr* param) { m_params.push_back(param); }
    const std::vector<Expr*>& get_params() { return m_params; }
    void accept(Visitor&);
    const std::string get_name() { return this->get_token().get_lexeme(); }
private:
    std::vector<Expr*> m_params;
};

class LetStmt : public Stmt {
public:
    LetStmt(Token token, IdentifierLiteral* identifier, Expr* expr, std::string filepath)
        : Stmt(token, filepath), m_identifier(identifier), m_assignment(expr) {}
    void accept(Visitor&);
    Expr* get_assignment() { return m_assignment; }
    IdentifierLiteral* get_identifier() { return m_identifier; }
private:
    IdentifierLiteral* m_identifier;
    Expr* m_assignment;
};

class VarStmt : public Stmt {
public:
    VarStmt(Token token, IdentifierLiteral* identifier, Expr* expr, std::string filepath)
        : Stmt(token, filepath), m_identifier(identifier), m_assignment(expr) {}
    Expr* get_assignment() { return m_assignment; }
    void accept(Visitor&);
    IdentifierLiteral* get_identifier() { return m_identifier; }
private:
    IdentifierLiteral* m_identifier;
    Expr* m_assignment;
};

class ImportStmt : public Stmt {
public:
    ImportStmt(Token token, std::string import_path, std::string filepath)
        : Stmt(token, filepath), m_import_path(import_path) {}
    void accept(Visitor&);
    void add_exclusion(Token token) { m_excluded_symboles.push_back(token); }
    void add_inclusion(Token token) { m_inclusive_symboles.push_back(token); }
    void set_alias(std::string alias) { m_alias = alias; }
    std::string get_alias() { return m_alias; }
    std::vector<Token> get_inclusion() { return m_inclusive_symboles; }
    std::string get_import_path() { return m_import_path; }
private:
    std::string m_import_path;
    std::string m_alias;
    std::vector<Token> m_excluded_symboles;
    std::vector<Token> m_inclusive_symboles;
};

class FunctionDecl : public Stmt {
public:
    FunctionDecl(Token token, std::string filepath) : Stmt(token, filepath) {}
    void add_param(IdentifierLiteral* param) { m_params.push_back(param); }
    void add_child(Stmt* stmt) { m_children.push_back(stmt); }
    std::string& get_name() { return m_name; }
    void set_name(std::string name) { m_name = name; }
    std::vector<Stmt*>& get_children() { return m_children; }
    std::vector<IdentifierLiteral*>& get_params() { return m_params; }
    std::string get_return_type() { return m_return_type; }
    void set_return_type(std::string type) { m_return_type = type; }
    void accept(Visitor&);
private:
    std::vector<IdentifierLiteral*> m_params;
    std::vector<Stmt*> m_children;
    std::string m_name;
    std::string m_return_type = "void";
};

class RangeExpr : public Expr {
public:
    RangeExpr(Token op, Expr* lower, Expr* upper, std::string filepath)
        : Expr(op, filepath, "range"), m_lower(lower), m_upper(upper) {
        Token step = Token{"1", 0,0,0, Integer};
        m_step = new IntLiteral(step, filepath);
    }
    void accept(Visitor&);
    Expr* get_lower() { return m_lower; }
    Expr* get_upper() { return m_upper; }
    Expr* get_step()  { return m_step;  }
    void  set_step(Expr* step) { m_step = step; }
private:
    Expr* m_lower;
    Expr* m_upper;
    Expr* m_step;
};

class InExpr : public Expr {
public:
    InExpr(Token op, Expr* lower, Expr* upper, std::string filepath)
        : Expr(op, filepath), m_left(lower), m_right(upper) {}
    void accept(Visitor&);
    Expr* get_left() { return m_left; }
    Expr* get_right() { return m_right; }
private:
    Expr* m_left;
    Expr* m_right;
};

class Visitor{
public:
    virtual void visit(BlockStmt*) = 0;
    virtual void visit(IfStmt*) = 0;
    virtual void visit(BinaryExpr*) = 0;
    virtual void visit(UnaryExpr*) = 0;
    virtual void visit(IntLiteral*) = 0;
    virtual void visit(TrueLiteral*) = 0;
    virtual void visit(FalseLiteral*) = 0;
    virtual void visit(CharLiteral*) = 0;
    virtual void visit(IdentifierLiteral*) = 0;
    virtual void visit(ForStmt*) = 0;
    virtual void visit(UnlessStmt*) = 0;
    virtual void visit(ElseStmt*) = 0;
    virtual void visit(LetStmt*) = 0;
    virtual void visit(VarStmt*) = 0;
    virtual void visit(ExprStmt*) = 0;
    virtual void visit(FunctionDecl*) = 0;
    virtual void visit(FunctionCall*) = 0;
    virtual void visit(ReturnStmt*) = 0;
    virtual void visit(ListLiteral*) = 0;
    virtual void visit(MemberExpr*) = 0;
    virtual void visit(StructDecl*) = 0;
    virtual void visit(StructInstance*) = 0;
    virtual void visit(StringLiteral*) = 0;
    virtual void visit(ImportStmt*) = 0;
    virtual void visit(RangeExpr*) = 0;
    virtual void visit(InExpr*) = 0;
    ~Visitor() {}
};
}
