#pragma once

#include <string>
#include <vector>
#include "ast_node.hpp"

namespace Kolibri {

class Module {
public:
    Module(std::string id, BlockStmt* root);
private:
    std::string m_id;
    BlockStmt* m_ast;
    std::vector<Module> children;
};

}
