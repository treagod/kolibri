#pragma once

#include <string>

namespace Kolibri {

enum TokenType {
    //
    LBrace = 0, RBrace, LAngBracket, RAngBracket, LParen, RParen, LBracket, RBracket, Quote, SingleQuote,
    Identifier, ClassMember, Newline, Eof, Invalid, Character,  String, Integer, Float, Number,
    // Operators
    Plus,PlusPlus, PlusEqual, Minus, MinusMinus, MinusEqual, Slash, SlashEqual, Asterix, AsterixEqual, Not, Equal, EqualEqual, NotEqual,
    Dot, Comma, DotDot, PipePipe, AmpAmp, Percent, Colon, QuestionMark,
    Less, LessEqual, Greater, GreaterEqual, EqualGreater,
    // Keywords
    If, Unless, Else, ElsIf, Class, Fn, Var, Let, Const, End, And, Bool, Or,
    Enum, Public, Private, Getter, Setter, In, While, For, Do, With, True, False,
    StringKw, Char, FloatKw, Int, Return, Break, Continue, Struct, Lib, Import, As,
    Except,

    SIZE_OF_ENUM
};

static const char* TokenNames[] = {
    "LBrace", "RBrace", "LAngBracket", "RAngBracket", "LParen", "RParen", "LBracket", "RBracket", "Quote", "SingleQuote", "Identifier", "ClassMember",
    "Newline", "Eof", "Invalid", "Character", "String", "Integer", "Float", "Number","Plus", "PlusPlus", "PlusEqual","Minus", "MinusMinus",
    "MinusEqual", "Slash", "SlashEqual", "Asterix", "AsterixEqual",
    "Not", "Equal", "EqualEqual", "NotEqual", "Dot", "Comma", "DotDot", "PipePipe", "AmpAmp", "Percent",
    "Colon", "QuestionMark", "Less", "LessEqual", "Greater", "GreaterEqual", "EqualGreater","If", "Unless", "Else", "ElsIf",
    "Class", "Fn", "Var", "Let", "Const", "End", "And", "Bool", "Or", "Enum", "Public", "Private", "Getter", "Setter",
    "In", "While", "For", "Do", "With", "True", "False", "StringKw", "Char", "FloatKw", "Int", "Return", "Break", "Continue",
    "Struct", "Lib", "Import", "As", "Except"
};

// statically check that the size of ColorNames fits the number of Colors
static_assert(sizeof(TokenNames)/sizeof(char*) == SIZE_OF_ENUM, "sizes dont match");
}
