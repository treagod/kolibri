#include "symbol.hpp"
namespace Kolibri {
 Symbol::Symbol(std::string name, SymbolType type) : m_name(name), m_type(type) {}
 Symbol::~Symbol() {}

 SymbolType Symbol::get_type() {
     return m_type;
 }

 const std::string& Symbol::get_name() {
     return m_name;
 }

 Symbol SymbolFactory::new_struct_symbol(std::string name) {
     return Symbol { name, Struct };
 }

 Symbol SymbolFactory::new_var_symbol(std::string name) {
     return Symbol { name, Var };
 }

 Symbol SymbolFactory::new_builtin_symbol(std::string name) {
     return Symbol { name, Struct };
 }
}
