#pragma once

#include "lexer.hpp"
#include "ast_node.hpp"

#include <map>
#include <vector>
#include <functional>

namespace Kolibri {

enum Precedence {
    Lowest = 0,
    Assignment,    // =
    Conditional,   // ?:
    LOr,    // ||
    LAnd,   // &&
    Equality,      // == !=
    // IS,            // is
    IN,
    Comparison,    // < > <= >=
    BOr,    // |
    BXor,   // ^
    BAnd,   // &
    Shift, // << >>
    Range,         // .. ..
    Term,          // + -
    Factor,        // * / %
    Unary,         // unary + - ! ~
    Call,          // . () []
    Primary
};

class Parser {
public:
    Parser(const std::string& source, const std::string& filepath);
    BlockStmt *parse_source();
    std::vector<Stmt*> parse_source_alt();
private:
    Stmt*       fn();
    Stmt*       strct();
    Stmt*       top_decleration();
    Stmt*       stmt();
    UnlessStmt* unless_stmt();
    VarStmt*    var_stmt();
    LetStmt*    let_stmt();
    ForStmt*    for_stmt();
    IfStmt*     if_stmt();
    ExprStmt*   expr_stmt();
    Stmt*       import_stmt();
    ReturnStmt* return_stmt();
    Expr*       parse_precedence(Precedence prec);
    Expr*       parse_expression();
    void        skip_newline();
    void        consume(TokenType token_type);
    void        consume(std::vector<TokenType>& token_types);
    void        init_prefixes();
    void        init_infixes();

    Lexer m_lexer;
    std::string m_filepath;
    Token m_current_token;
    Token m_next_token;
    std::map<TokenType, std::function<Expr*(void)>> m_prefixes;
    std::map<TokenType, std::function<Expr*(Expr*)>> m_infixes;
};
}
