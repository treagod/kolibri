#include "ast_node.hpp"

namespace Kolibri {

Expr::Expr(Token token, std::string filepath) : m_token(token), m_filepath(filepath) {}
Expr::Expr(Token token, std::string filepath, std::string type)
    : m_token(token), m_filepath(filepath), m_type(type) {}
Expr::~Expr() {};
Token Expr::get_token() { return m_token; }
const std::string& Expr::get_filepath() { return m_filepath; }
void Expr::set_type(std::string type) { m_type = type; }
const std::string& Expr::get_type() { return m_type; }

void BlockStmt::accept(Visitor& v) {
    v.visit(this);
}

void ForStmt::accept(Visitor& v) {
    v.visit(this);
}


void LetStmt::accept(Visitor& v) {
    v.visit(this);
}


void VarStmt::accept(Visitor& v) {
    v.visit(this);
}


void BinaryExpr::accept(Visitor& v) {
    v.visit(this);
}

void UnaryExpr::accept(Visitor& v) {
    v.visit(this);
}

void IntLiteral::accept(Visitor& v) {
    v.visit(this);
}

void FunctionDecl::accept(Visitor& v) {
    v.visit(this);
}

void IdentifierLiteral::accept(Visitor& v) {
    v.visit(this);
}

void CharLiteral::accept(Visitor& v) {
    v.visit(this);
}

void TrueLiteral::accept(Visitor& v) {
    v.visit(this);
}

void FalseLiteral::accept(Visitor& v) {
    v.visit(this);
}

void UnlessStmt::accept(Visitor& v) {
    v.visit(this);
}

void ElseStmt::accept(Visitor& v) {
    v.visit(this);
}

void ExprStmt::accept(Visitor& v) {
    v.visit(this);
}

void IfStmt::accept(Visitor& v) {
    v.visit(this);
}

void FunctionCall::accept(Visitor& v) {
    v.visit(this);
}

void ReturnStmt::accept(Visitor& v) {
    v.visit(this);
}

void ListLiteral::accept(Visitor& v) {
    v.visit(this);
}

void MemberExpr::accept(Visitor& v) {
    v.visit(this);
}

void StructDecl::accept(Visitor& v) {
    v.visit(this);
}

void StructInstance::accept(Visitor& v) {
    v.visit(this);
}

void StringLiteral::accept(Visitor& v) {
    v.visit(this);
}

void ImportStmt::accept(Visitor& v) {
    v.visit(this);
}

void RangeExpr::accept(Visitor& v) {
    v.visit(this);
}

void InExpr::accept(Visitor& v) {
    v.visit(this);
}

}
