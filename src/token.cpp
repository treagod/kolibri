#include "token.hpp"

namespace Kolibri {

Token::Token(std::string lexeme, int position, int line, int column, TokenType token_type) 
    : m_lexeme(lexeme), m_position(position), m_line(line), m_column(column), m_token_type(token_type) {}

std::string Token::get_lexeme() const { return m_lexeme; }

int Token::get_position() const { return m_position; }

int Token::get_line() const { return m_line; }

int Token::get_column() const { return m_column; }

TokenType Token::get_token_type() const { return m_token_type; }

std::ostream& operator<< (std::ostream& stream, const Token& token) {
    stream << "<Token name=\"";
    stream << token.get_lexeme();
    stream << "\" ";
    stream << "position=\"" << token.get_line();
    stream << ":" << token.get_column() << "\" ";
    stream << "type=\"" << TokenNames[token.get_token_type()] << "\">\n";
    
    return stream;
}

}
