#pragma once

#include "token_type.hpp"

#include <iostream>
#include <string>

namespace Kolibri {

class Token {
public:
    Token(std::string lexeme, int position, int line, int column, TokenType token_type);
    std::string get_lexeme() const ;
    int get_position() const ;
    int get_line() const ;
    int get_column() const ;
    TokenType get_token_type() const ;


    friend std::ostream& operator<< (std::ostream& stream, const Token& token);
private:
    TokenType m_token_type;
    std::string m_lexeme;
    int m_position;
    int m_line;
    int m_column;
};

}
