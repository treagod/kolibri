#pragma once

#include "ast_node.hpp"
#include "module.hpp"

namespace Kolibri {
class Types {
    BlockStmt* m_block_stmt;
    Module m_module;
public:
    Module resolve_module();
};
}
