#include "util.hpp"
#include <fstream>
#include <sstream>
#include <iostream>

namespace Kolibri {
std::string read_file(std::string path) {
    std::ifstream file_stream(path);
    std::string line;
    std::stringstream content;

    if (file_stream.is_open()) {

        while (getline(file_stream, line)) {
            content << line << "\n";
        }

        file_stream.close();
    } else {
        std::cerr << "Could not open file " << path << "\n";
        exit(1);
    }

    return content.str();
}
}
