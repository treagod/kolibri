#include "parser.hpp"

#include <iostream>
#include <cstdarg>

namespace Kolibri {

Precedence getPriority(TokenType token_type) {
    switch(token_type) {
    case Equal:
    case PlusEqual:
    case MinusEqual:
    case AsterixEqual:
    case SlashEqual:
        return Assignment;
    case Plus:
    case Minus:
        return Term;
    case Asterix:
    case Slash:
    case Percent:
        return Factor;
    case Or:
        return LOr;
    case And:
        return LAnd;
    case Less:
    case LessEqual:
    case Greater:
    case GreaterEqual:
    case EqualEqual:
        return Comparison;
    case Dot:
    case LParen:
    case LBrace:
        return Call;
    case DotDot:
        return Range;
    case In:
        return IN;
    default:
        return Lowest;
    }
}

#define RETURN_LITERAL(token_type, lit) auto literal = \
    new lit(this->m_current_token, this->m_filepath);consume(token_type);return literal

#define RETURN_BINARY(token_type) Token op = m_current_token; \
    consume(token_type); \
    this->skip_newline(); \
    Expr* right = parse_precedence(getPriority(token_type)); \
    return new BinaryExpr(op, left, right, this->m_filepath);

Parser::Parser(const std::string& source, const std::string& filepath)
    : m_lexer(Lexer(source)), m_current_token(m_lexer.get_next_token()),
      m_next_token(m_lexer.get_next_token()), m_filepath(filepath) {
    init_prefixes();
    init_infixes();
}

std::vector<Stmt*> Parser::parse_source_alt() {
    std::vector<Stmt*> stmts;
    while (m_current_token.get_token_type() != Eof) {
        stmts.push_back(top_decleration());
    }
    return stmts;
}

void Parser::init_prefixes() {
    m_prefixes[Minus]      = [this]() -> Expr* {
            auto unary_token = m_current_token;
            consume(Minus);
            return new UnaryExpr(unary_token, parse_precedence(Unary), m_filepath);
};
    m_prefixes[Plus]      = [this]() -> Expr* {
            auto unary_token = m_current_token;
            consume(Plus);
            return new UnaryExpr(unary_token, parse_precedence(Unary), m_filepath);
};
    m_prefixes[Not]      = [this]() -> Expr* {
            auto unary_token = m_current_token;
            consume(Not);
            return new UnaryExpr(unary_token, parse_precedence(Unary), m_filepath);
};
    m_prefixes[Integer]    = [this]() -> Expr* { RETURN_LITERAL(Integer, IntLiteral); };
    m_prefixes[Identifier] = [this]() -> Expr* { RETURN_LITERAL(Identifier, IdentifierLiteral); };
    m_prefixes[Character]  = [this]() -> Expr* { RETURN_LITERAL(Character, CharLiteral); };
    m_prefixes[True]  = [this]() -> Expr* { RETURN_LITERAL(True, TrueLiteral); };
    m_prefixes[False]  = [this]() -> Expr* { RETURN_LITERAL(False, FalseLiteral); };
    m_prefixes[String]  = [this]() -> Expr* { RETURN_LITERAL(String, StringLiteral); };
    m_prefixes[LParen]     = [this]() -> Expr* {
            auto list_start = m_current_token;
            consume(LParen);
            Expr* expr = parse_expression();
            if (m_current_token.get_token_type() == Comma) {
            auto list_literal = new ListLiteral(list_start, m_filepath);
            while(m_current_token.get_token_type() == Comma) {
            list_literal->add_item(expr);
            consume(Comma);
            expr = parse_expression();
}

            consume(RParen);

            return list_literal;
}
            consume(RParen);
            return expr;
};

}

void Parser::init_infixes() {
    m_infixes[Dot] = [this](Expr* left) -> Expr* {
        auto member_token = m_current_token;
        consume(Dot);
        Expr* expr = new IdentifierLiteral(m_current_token, m_filepath);
        consume(Identifier);
        return new MemberExpr(member_token, m_filepath, left, expr);
    };
    m_infixes[Plus] = [this](Expr* left) -> Expr* { RETURN_BINARY(Plus) };
    m_infixes[Less] = [this](Expr* left) -> Expr* { RETURN_BINARY(Less) };
    m_infixes[LessEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(LessEqual) };
    m_infixes[GreaterEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(GreaterEqual) };
    m_infixes[Greater] = [this](Expr* left) -> Expr* { RETURN_BINARY(Greater) };
    m_infixes[Minus] = [this](Expr* left) -> Expr* { RETURN_BINARY(Minus) };
    m_infixes[Asterix] = [this](Expr* left) -> Expr* { RETURN_BINARY(Asterix) };
    m_infixes[Slash] = [this](Expr* left) -> Expr* { RETURN_BINARY(Slash) };
    m_infixes[Percent] = [this](Expr* left) -> Expr* { RETURN_BINARY(Percent) };
    m_infixes[And] = [this](Expr* left) -> Expr* { RETURN_BINARY(And) };
    m_infixes[Or] = [this](Expr* left) -> Expr* { RETURN_BINARY(Or) };
    m_infixes[Equal] = [this](Expr* left) -> Expr* { RETURN_BINARY(Equal) };
    m_infixes[PlusEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(PlusEqual) };
    m_infixes[MinusEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(MinusEqual) };
    m_infixes[SlashEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(SlashEqual) };
    m_infixes[AsterixEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(AsterixEqual) };
    m_infixes[EqualEqual] = [this](Expr* left) -> Expr* { RETURN_BINARY(EqualEqual) };
    m_infixes[DotDot] = [this](Expr* lower_limit) -> Expr* {
        auto dotdot_token = m_current_token;
        consume(DotDot);
        Expr* upper_limit = parse_expression();
        auto range_expr = new RangeExpr(dotdot_token,
                                        lower_limit, upper_limit, m_filepath);
        if(m_current_token.get_token_type() == Comma) {
            consume(Comma);
            range_expr->set_step(parse_expression());
        }

        return range_expr;
    };
    m_infixes[In] = [this](Expr* left) -> Expr* {
        auto in_token = m_current_token;
        consume(In);
        Expr* right = parse_expression();
        auto in_expr = new InExpr(in_token,
                                  left, right, m_filepath);

        return in_expr;

    };
    m_infixes[LParen] = [this](Expr* left) -> Expr* {
        auto function_call = new FunctionCall(left->get_token(), this->m_filepath);
        consume(LParen);
        while (m_current_token.get_token_type() != RParen) {
            auto param = this->parse_precedence(Lowest);
            function_call->add_param(param);
            if (m_current_token.get_token_type() != Comma) break;
            consume(Comma);
        }
        consume(RParen);

        return function_call;
    };
    m_infixes[LBrace] = [this](Expr* left) -> Expr* {
        bool first = true;
        auto struct_instance = new StructInstance(left->get_token(), m_filepath);
        consume(LBrace);
        while (m_current_token.get_token_type() != Eof && m_current_token.get_token_type() != RBrace) {
            if (first) {
                first = false;
            } else {
                consume(Comma);
            }
            auto member_name = m_current_token;
            consume(Identifier);
            consume(Colon);
            auto member_value = parse_expression();
            struct_instance->add_member(member_name, member_value);
        }
        consume(RBrace);
        return struct_instance;
    };
}

BlockStmt* Parser::parse_source() {
    auto tud = new BlockStmt(this->m_filepath);
    while (m_current_token.get_token_type() != Eof) {
        tud->add_child(top_decleration());
    }
    return tud;
}

Stmt* Parser::top_decleration() {
    Stmt* stmt;
    skip_newline();
    switch(m_current_token.get_token_type()) {
    case Fn:
        stmt = fn();
        break;
    case Struct:
        stmt = strct();
        break;
    case Let:
    case Var:
    case For:
    case Import:
        stmt = this->stmt();
        break;
    case Lib:
        std::cerr << "asda\n";
        exit(1);
    default:
        std::cerr << "Invalid top level decleration\n";
        exit(1);
    }
    skip_newline();
    return stmt;
}

Stmt* Parser::strct() {
    consume(Struct);
    auto struct_identifier = m_current_token;
    consume(Identifier);
    auto struct_decl = new StructDecl(struct_identifier, m_filepath);
    skip_newline();
    while(m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof) {
        auto type_token = m_current_token;
        consume(Identifier);
        auto name_token = m_current_token;
        consume(Identifier);
        struct_decl->add_member(type_token, name_token);
        skip_newline();
    }

    consume(End);

    return struct_decl;
}

Stmt* Parser::fn() {
    auto fn_stmt = new FunctionDecl(m_current_token, this->m_filepath);
    consume(Fn);
    fn_stmt->set_name(m_current_token.get_lexeme());
    consume(Identifier);
    consume(LParen);
    while(m_current_token.get_token_type() != RParen) {
        bool is_mutable = false;
        auto param_type = m_current_token;
        consume(Identifier);
        if (m_current_token.get_token_type() == Asterix) {
            is_mutable = true;
            consume(Asterix);
        }
        auto param = new IdentifierLiteral(m_current_token, this->m_filepath);
        param->set_type(param_type.get_lexeme());
        consume(Identifier);
        if(is_mutable) param->set_mutable();
        fn_stmt->add_param(param);
        if (m_current_token.get_token_type() == Comma) {
            consume(Comma);
        } else {
            break;
        }
    }
    consume(RParen);
    if (m_current_token.get_token_type() == Colon) {
        consume(Colon);
        fn_stmt->set_return_type(m_current_token.get_lexeme());
        consume(Identifier);
    }
    consume(Newline);
    while(m_current_token.get_token_type() != Eof && m_current_token.get_token_type() != End) {
        fn_stmt->add_child(stmt());
    }

    consume(End);

    return fn_stmt;
}

Stmt* Parser::stmt() {
    Stmt* stmt;
    skip_newline();

    switch(m_current_token.get_token_type()) {
    case Var:
        stmt = var_stmt();
        break;
    case Let:
        stmt = let_stmt();
        break;
    case For:
        stmt = for_stmt();
        break;
    case If:
        stmt = if_stmt();
        break;
    case Unless:
        stmt = unless_stmt();
        break;
    case Return:
        stmt = return_stmt();
        break;
    case Import:
        stmt = import_stmt();
        break;
    default:
        stmt = expr_stmt();
    }
    skip_newline();
    return stmt;
}

Stmt* Parser::import_stmt() {
    auto import_token = m_current_token;
    consume(Import);
    auto path_token = m_current_token;
    consume(String);

    auto import_stmt = new ImportStmt(import_token, path_token.get_lexeme(), m_filepath);

    if (m_current_token.get_token_type() == As) {
        consume(As);
        auto alias_token = m_current_token;
        consume(Identifier);
        import_stmt->set_alias(alias_token.get_lexeme());
    }

    if (m_current_token.get_token_type() == For) {
        consume(For);
        auto symbol_token = m_current_token;
        consume(Identifier);
        import_stmt->add_inclusion(symbol_token);
        while(m_current_token.get_token_type() == Comma) {
            consume(Comma);
            symbol_token = m_current_token;
            consume(Identifier);
            import_stmt->add_inclusion(symbol_token);
        }
    } else if(m_current_token.get_token_type() == Except) {
        consume(Except);
        auto exclusion_token = m_current_token;
        consume(Identifier);
        import_stmt->add_exclusion(exclusion_token);
        while(m_current_token.get_token_type() == Comma) {
            consume(Comma);
            exclusion_token = m_current_token;
            consume(Identifier);
            import_stmt->add_exclusion(exclusion_token);
        }

    }

    consume(Newline);

    return import_stmt;
}

ReturnStmt* Parser::return_stmt() {
    auto return_token = m_current_token;
    consume(Return);
    if (m_current_token.get_token_type() == Newline) {
        return new ReturnStmt(return_token, nullptr, m_filepath);
    }
    return new ReturnStmt(return_token, parse_expression(), m_filepath);
}

ForStmt* Parser::for_stmt() {
    auto for_token = m_current_token;
    consume(For);
    Expr* expr = parse_expression();

    auto for_stmt = new ForStmt(for_token, expr, m_filepath);

    while(m_current_token.get_token_type() != Eof && m_current_token.get_token_type() != End) {
        for_stmt->add_child(stmt());
    }

    consume(End);

    return for_stmt;
}

IfStmt* Parser::if_stmt() {
    auto if_token = m_current_token;
    consume(If);
    Expr* condition = parse_expression();
    std::vector<TokenType> v = {Do, Newline};
    consume(v);

    auto if_stmt = new IfStmt(if_token, condition, this->m_filepath);

    while (m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof &&
           m_current_token.get_token_type() != Else && m_current_token.get_token_type() != ElsIf) {
        if_stmt->add_child(stmt());
    }

    while (m_current_token.get_token_type() == ElsIf && m_current_token.get_token_type() != Eof &&
           m_current_token.get_token_type() != Else) {
        auto else_if_token = m_current_token;
        consume(ElsIf);
        auto condition = parse_expression();
        auto else_if_stmt = new IfStmt(else_if_token, condition, this->m_filepath);

        while (m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof &&
               m_current_token.get_token_type() != Else && m_current_token.get_token_type() != ElsIf) {
            else_if_stmt->add_child(stmt());
        }

        if_stmt->add_else_if(else_if_stmt);
    }

    if(m_current_token.get_token_type() == Else) {
        auto else_stmt = new ElseStmt(m_current_token, this->m_filepath);
        consume(Else);

        while (m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof) {
            else_stmt->add_child(stmt());
        }

        if_stmt->set_else_stmt(else_stmt);
    }


    consume(End);
    return if_stmt;
}

UnlessStmt* Parser::unless_stmt() {
    auto unless_token = m_current_token;
    consume(Unless);
    Expr* condition = parse_expression();
    std::vector<TokenType> v = {Do, Newline};
    consume(v);

    auto unless_stmt = new UnlessStmt(unless_token, condition, this->m_filepath);

    while (m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof &&
           m_current_token.get_token_type() != Else) {
        unless_stmt->add_child(stmt());
    }

    if(m_current_token.get_token_type() == Else) {
        auto else_stmt = new ElseStmt(m_current_token, this->m_filepath);
        consume(Else);

        while (m_current_token.get_token_type() != End && m_current_token.get_token_type() != Eof) {
            else_stmt->add_child(stmt());
        }

        unless_stmt->set_else_stmt(else_stmt);
    }

    consume(End);
    return unless_stmt;
}

ExprStmt* Parser::expr_stmt() {
    return new ExprStmt(parse_expression(), this->m_filepath);
}

VarStmt* Parser::var_stmt() {
    auto assign_token = m_current_token;
    consume(Var);
    auto ident_stmt = new IdentifierLiteral(m_current_token, this->m_filepath);
    ident_stmt->set_mutable();
    consume(Identifier);
    consume(Equal);
    Expr* expr = parse_expression();

    return new VarStmt(assign_token, ident_stmt, expr, this->m_filepath);
}

LetStmt* Parser::let_stmt() {
    auto assign_token = m_current_token;
    consume(Let);
    auto ident_stmt = new IdentifierLiteral(m_current_token, this->m_filepath);
    consume(Identifier);
    consume(Equal);
    Expr* expr = parse_expression();

    return new LetStmt(assign_token, ident_stmt, expr, this->m_filepath);
}

Expr* Parser::parse_expression() {
    return parse_precedence(Lowest);
}

Expr* Parser::parse_precedence(Precedence prec) {
    Expr* left;
    if((m_prefixes.count(m_current_token.get_token_type()) > 0)) {
        left = m_prefixes[m_current_token.get_token_type()]();
    } else {
        std::cerr << "Syntax error at " << m_current_token.get_line();
        std::cerr << ":" << m_current_token.get_column() << ".\n";
        std::cerr << "Expected an expression. Got ";
        std::cerr << TokenNames[m_current_token.get_token_type()] << ".\n";
        exit(1);
    }

    auto current_type = m_current_token.get_token_type();


    while (!(current_type == Newline || current_type == Eof) && prec < getPriority(m_current_token.get_token_type())) {
        if((m_infixes.count(m_current_token.get_token_type()) > 0)) {
            left = m_infixes[m_current_token.get_token_type()](left);
        } else {
            std::cerr << m_current_token << " something went wrong\n";
            exit(1);
        }
    }

    return left;
}

void Parser::skip_newline() {
    while(m_current_token.get_token_type() == Newline) {
        consume(Newline);
    }
}

void Parser::consume(TokenType token_type) {
    if (m_current_token.get_token_type() != token_type) {
        // Syntax error
        std::cerr << "Syntax error at " << m_current_token.get_line();
        std::cerr << ":" << m_current_token.get_column() << ".\n";
        std::cerr << "Expected " << TokenNames[token_type] << ". Got ";
        std::cerr << TokenNames[m_current_token.get_token_type()] << ".\n";
        exit(1);
    }

    m_current_token = m_next_token;

    if (m_current_token.get_token_type() != Eof) {
        m_next_token = m_lexer.get_next_token();
    }
}

void Parser::consume(std::vector<TokenType>& token_types) {
    bool is_valid = false;
    for(const auto ttype : token_types) {
        if (m_current_token.get_token_type() == ttype) is_valid = true;
    }

    if(!is_valid) {
        bool first = true;
        std::cerr << "Syntax error at " << m_current_token.get_line();
        std::cerr << ":" << m_current_token.get_column() << ".\n";
        std::cerr << "Expected ";
        for(int i = 0; i < (token_types.size() - 1); i++) {
            auto ttype = token_types.at(i);
            if (first) {
                std::cerr << TokenNames[ttype];
                first = false;
            } else {
                std::cerr << ", " << TokenNames[ttype];
            }
        }
        std::cerr << " or " << TokenNames[token_types.at(token_types.size() - 1)];
        std::cerr << ". Got ";
        std::cerr << TokenNames[m_current_token.get_token_type()] << ".\n";
        exit(1);
    }

    m_current_token = m_next_token;

    if (m_current_token.get_token_type() != Eof) {
        m_next_token = m_lexer.get_next_token();
    }
}

}
