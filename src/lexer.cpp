#include "lexer.hpp"

#include <sstream>
#include <cctype>
#include <map>
#include <iostream>

namespace Kolibri {

static const std::map<std::string, TokenType> keywords{
    {"if", If}, {"var", Var}, {"unless", Unless}, {"else", Else}, {"elsif", ElsIf},
    {"class", Class}, {"fn", Fn}, {"let", Let}, {"const", Const}, {"end", End}, {"and", And},
    {"or", Or}, {"enum", Enum}, {"public", Public}, {"private", Private}, {"getter", Getter},
    {"setter", Setter}, {"while", While}, {"for", For}, {"do", Do}, {"with", With}, {"true", True},
    {"false", False}, {"return", Return}, {"break", Break}, {"continue", Continue},
    {"struct", Struct}, {"lib", Lib}, {"import", Import}, {"as", As}, {"except", Except},
    {"in", In}
};

Lexer::Lexer(std::string source) : m_source(source) {
    m_line = 1;
    m_column = 1;
    m_position = 0;
    m_current_char = m_source[m_position];
}

Token Lexer::get_next_token() {
    skip_whitespace_and_comment();
    int pos = m_position;
    int line = m_line;
    int col = m_column;

    if (isalpha(m_current_char)) {
        return next_key_var();
    }

    if (isdigit(m_current_char)) {
        return next_number();
    }

    if (m_current_char == '@') {
        return next_class_member();
    }

    if (m_current_char == '\'') {
        return next_char();
    }

    if (m_current_char == '=') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token("==", pos, line, col, EqualEqual);
        }

        if (m_current_char == '>') {
            advance();
            return Token("=>", pos, line, col, EqualGreater);
        }

        return Token("=", pos, line, col, Equal);
    }

    if (m_current_char == '.') {
        advance();
        if (m_current_char == '.') {
            advance();
            return Token("..", pos, line, col, DotDot);
        }
        return Token(".", pos, line, col, Dot);
    }

    if (m_current_char == '<') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token("<=", pos, line, col, LessEqual);
        }
        return Token("<", pos, line, col, Less);
    }

    if (m_current_char == '>') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token(">=", pos, line, col, GreaterEqual);
        }
        return Token(">", pos, line, col, Greater);
    }

    if (m_current_char == '!') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token("!=", pos, line, col, NotEqual);
        }
        advance();
        return Token("!", pos, line, col, Not);
    }

    if (m_current_char == '{') {
        advance();
        return Token("{", pos, line, col, LBrace);
    }

    if (m_current_char == '}') {
        advance();
        return Token("}", pos, line, col, RBrace);
    }

    if (m_current_char == '[') {
        advance();
        return Token("[", pos, line, col, LBracket);
    }

    if (m_current_char == ']') {
        advance();
        return Token("]", pos, line, col, RBracket);
    }

    if (m_current_char == '<') {
        advance();
        return Token("<", pos, line, col, LAngBracket);
    }

    if (m_current_char == '>') {
        advance();
        return Token(">", pos, line, col, RAngBracket);
    }

    if (m_current_char == '(') {
        advance();
        return Token("(", pos, line, col, LParen);
    }

    if (m_current_char == ')') {
        advance();
        return Token(")", pos, line, col, RParen);
    }

    if (m_current_char == '"') {
        advance();
        return next_string();
        //return Token("\"", pos, line, col, Quote);
    }

    if (m_current_char == '\'') {
        advance();
        return Token("'", pos, line, col, SingleQuote);

    }

    if (m_current_char == '+') {
        advance();
        if (m_current_char == '+') {
            advance();
            return Token("++", pos, line, col, PlusPlus);
        }
        if (m_current_char == '=') {
            advance();
            return Token("+=", pos, line, col, PlusEqual);
        }
        return Token("+", pos, line, col, Plus);
    }

    if (m_current_char == '-') {
        advance();
        if (m_current_char == '-') {
            advance();
            return Token("--", pos, line, col, MinusMinus);
        }
        if (m_current_char == '=') {
            advance();
            return Token("-=", pos, line, col, MinusEqual);
        }
        return Token("-", pos, line, col, Minus);
    }

    if (m_current_char == '*') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token("*=", pos, line, col, AsterixEqual);
        }
        return Token("*", pos, line, col, Asterix);
    }

    if (m_current_char == '/') {
        advance();
        if (m_current_char == '=') {
            advance();
            return Token("/=", pos, line, col, SlashEqual);
        }
        return Token("/", pos, line, col, Slash);
    }

    if (m_current_char == ':') {
        advance();
        return Token(":", pos, line, col, Colon);
    }

    if (m_current_char == '%') {
        advance();
        return Token("%", pos, line, col, Percent);
    }

    if (m_current_char == ',') {
        advance();
        return Token(",", pos, line, col, Comma);
    }

    if (m_current_char == '?') {
        advance();
        return Token("?", pos, line, col, QuestionMark);
    }

    if (m_current_char == '&' && peek() == '&') {
        advance(); advance();
        return Token("&&", pos, line, col, And);
    }

    if (m_current_char == '|' && peek() == '|') {
        advance(); advance();
        return Token("||", pos, line, col, Or);
    }


    if (m_current_char == '\n') {
        advance();
        return Token("\\n", pos, line, col, Newline);
    }

    if (m_current_char == '\0') {
        return Token("\\0", pos, line, col, Eof);
    }

    return Token("Invalid", pos, line, col, Invalid);
}

Token Lexer::next_string() {
    int pos = m_position;
    int line = m_line;
    int col = m_column;

    if(m_current_char == '"') {
        advance();
        return Token("", pos, line, col, String);
    }

    std::stringstream str;
    while (!(m_current_char != '\\' && peek() == '"') && m_current_char != '\0') {
        str << m_current_char;
        advance();
    }
    str << m_current_char;
    advance();
    advance();
    return Token(str.str(), pos, line, col, String);
}

Token Lexer::next_char() {
    advance();
    char c = m_current_char;
    int pos = m_position;
    int line = m_line;
    int col = m_column;
    advance();

    if (m_current_char != '\'') {
        std::cerr << "A char literal must not consist of more than two character\n";
        exit(1);
    }
    advance();

    return Token(std::string(1, c), pos, line, col, Character);
}

Token Lexer::next_number() {
    std::stringstream lexeme_builder;
    bool is_float = false;
    int pos = m_position;
    int line = m_line;
    int col = m_column;

    while(isdigit(m_current_char) || m_current_char == '.') {
        if(m_current_char == '.') {
            if (peek() == '.') break; // Range operator
            if(isalpha(peek())) break; // member operation
            if (is_float) {
                // Error _ placeholder for now
                std::cerr << "Number has already a '.'\n";
                exit(1);
            }
            is_float = true;
        }
        lexeme_builder << m_current_char;
        advance();
    }

    return is_float ? Token(lexeme_builder.str(), pos, line, col, Float) :
                      Token(lexeme_builder.str(), pos, line, col, Integer);
}

Token Lexer::next_key_var() {
    std::stringstream lexeme_builder;
    int pos = m_position;
    int line = m_line;
    int col = m_column;
    while(isalnum(m_current_char) || m_current_char == '_') {
        lexeme_builder << m_current_char;
        advance();
    }

    if (m_current_char == '?') {
        lexeme_builder << m_current_char;
        advance();
    }
    std::string lexeme = lexeme_builder.str();

    if (keywords.find(lexeme) != keywords.end()) {
        return Token(lexeme, pos, line, col, keywords.at(lexeme));
    }

    return Token(lexeme, pos, line, col, Identifier);
}

Token Lexer::next_class_member() {
    std::stringstream lexeme_builder;
    int pos = m_position;
    int line = m_line;
    int col = m_column;
    lexeme_builder << '@';
    advance();
    while(isalnum(m_current_char)) {
        lexeme_builder << m_current_char;
        advance();
    }

    return Token(lexeme_builder.str(), pos, line, col, ClassMember);
}

void Lexer::advance() {
    if (m_current_char == '\n') {
        m_line++;
        m_column = 1;
    } else {
        m_column++;
    }

    if(m_source.length() <= ++m_position) {
        m_current_char = '\0';
    } else {
        m_current_char = m_source[m_position];
    }
}

char Lexer::peek(int offset) {
    return m_source[m_position + offset];
}

void Lexer::skip_whitespace_and_comment() {
    while (current_char_is_comment() || current_char_is_whitespace()) {
        if (current_char_is_comment()) {
            while (m_current_char != '\n') {
                advance();
            }
        }
        advance();
    }
}

bool Lexer::current_char_is_comment() {
    return m_current_char == '#';
}

bool Lexer::current_char_is_whitespace() {
    return (m_current_char == ' '
            || m_current_char == '\t');
}

}
