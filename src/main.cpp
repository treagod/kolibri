#include "parser.hpp"
#include "module.hpp"
#include "util.hpp"
#include "types.hpp"
#include "visitors/c_printer.hpp"
#include "visitors/ast_printer.hpp"
#include "visitors/function_table.hpp"
#include "visitors/type_checker.hpp"

#include <fstream>
#include <sstream>


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#else
#include <cstdlib>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#endif




struct KolibriConfig {
    bool debug = false;
    bool release = false;
    bool ast_dump = false;
    bool run = false;
    std::string filepath = "";
};

int path_is_directory (const char* path);

void delete_folder_tree (const char* directory_name) {
    DIR*            dp;
    struct dirent*  ep;
    char            p_buf[512] = {0};

    dp = opendir(directory_name);

    while ((ep = readdir(dp)) != NULL) {
        if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) continue;
        sprintf(p_buf, "%s/%s", directory_name, ep->d_name);
        if (path_is_directory(p_buf))
            delete_folder_tree(p_buf);
        else
            unlink(p_buf);
    }

    closedir(dp);
    rmdir(directory_name);
}

int path_is_directory (const char* path) {
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#else
    struct stat s_buf;

    if (stat(path, &s_buf))
        return 0;

    return S_ISDIR(s_buf.st_mode);
#endif
}

int create_executable(Kolibri::CPrinter& printer, const KolibriConfig& config) {
    std::string compiled_files_dir = "./.colibri_files";
    std::string bin_file_dir = "./bin";

    struct stat sb;

    if (!path_is_directory(compiled_files_dir.c_str())) {
        int result = mkdir(compiled_files_dir.c_str(), 0777);
        if (result) {
            std::cerr <<"asdasd\n";
            exit(1);
        }

    }

    if (!path_is_directory(compiled_files_dir.c_str())) {
        int result = mkdir(bin_file_dir.c_str(), 0777);
        if (result) {
            std::cerr <<"asdasd\n";
            exit(1);
        }

    }

    // Compiling
    // Todo: Extract to own function
    std::string base_filename = config.filepath.substr(config.filepath.find_last_of("/\\") + 1);
    auto pos = base_filename.rfind('.');
    if (pos != std::string::npos) {
        std::string without_extension = base_filename.substr(0, pos);
        base_filename = without_extension + ".c";
        std::ofstream c_output;
        c_output.open(compiled_files_dir + "/" + base_filename);
        c_output << printer.emit_c();
        c_output.close();
        auto error_gcc = std::system("which gcc &> /dev/null");
        auto error_clang = std::system("which clang &> /dev/null");
        std::string command;
        if (!error_clang) {
            command = "clang ";
        } else if (!error_gcc) {
            command = "gcc ";
        } else {
            std::cerr << "No familiar C compiler found. Put clang or gcc in your path.\n";
            exit(1);
            // Todo: specify config to set compiler path
        }

        std::string executable_path = bin_file_dir + "/" + without_extension;

        if (!error_clang || !error_gcc) {
            command += compiled_files_dir + "/* -o " + executable_path;
            if (config.release) {
                command += " -O2";
            }
            std::system(command.c_str());

            if (config.run) {
                std::system(executable_path.c_str());
            }
        }

        if (path_is_directory(compiled_files_dir.c_str())) {
            delete_folder_tree(compiled_files_dir.c_str());
        }
    }

    return 0;
}

const std::string exe_option(std::string arg_name, std::string message);

// Todo: make alternatve option (i.e. --run, -r)
const std::string help_message() {
    return std::string("Kolibri Language - Development Version\n\n") +
            "Usage: kolibri [options] <entry_file>\n" +
            "Options:\n" +
            exe_option("ast_dump", "Prints out the AST for the translation unit. Does not compile the source file.") +
            exe_option("run", "Compiles the application and immediately runs it.") +
            exe_option("release", "Compiles in release mode. Might improve speed and decrease binary size.");
}

const std::string exe_option(std::string arg_name, std::string message) {
    std::string option_text;
    option_text += "\t--";
    option_text += arg_name;
    option_text += ",\t\t";
    option_text += message;
    option_text += "\n";
    return option_text;
}

const std::string get_absolute_path(std::string path) {
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    char full_path[MAX_PATH];
    GetFullPathName(path, MAX_PATH, full_path, NULL);
    return full_path;
#else
    char full_path[PATH_MAX];
    realpath(path.c_str(), full_path);
    return full_path;
#endif
}

void configure(int argc, char* argv[], KolibriConfig& config) {
    bool file_specified = false;
    for(int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if (arg[0] != '-' ) {
            if (file_specified) {
                std::cerr << "Error: More than one file specified.\n";
                exit(1);
            }
            config.filepath = get_absolute_path(arg);
            file_specified = true;
        } else if (arg[0] == '-' && arg[1] == '-'){
            std::string arg_name = arg.substr(2);
            if (arg_name == "debug") {
                config.debug = true;
            } else if (arg_name == "help") {
                std::cout << help_message();
                exit(1);
            } else if (arg_name == "ast_dump") {
                config.ast_dump = true;
            } else if (arg_name == "release") {
                config.release = true;
            } else if(arg_name == "run") {
                config.run = true;
            } else {
                std::cerr << "Unknown option " << arg_name << ". --help for more info.\n";
                exit(1);
            }
        } else if (arg[0] == '-') {
            std::string arg_name = arg.substr(1);

            if (arg_name == "r") {
                config.run = true;
            }
        }
    }
}

int main(int argc, char* argv[]) {
    KolibriConfig config;

    configure(argc, argv, config);

    if (config.filepath == "") {
        std::cerr << "Error: No input file specified.\n\n";
        std::cerr << help_message();
        exit(1);
    }
    std::string code = Kolibri::read_file(config.filepath);

    Kolibri::Parser parser(code, config.filepath);

    auto node = parser.parse_source();

    Kolibri::Module mod = { config.filepath, node };

    Kolibri::FunctionTable func_table;
    node->accept(func_table);

    //Kolibri::TypeChecker type_checker {func_table};
    //node->accept(type_checker);


    exit(1);
    Kolibri::CPrinter cprinter;
    node->accept(cprinter);

    if (config.debug) {
        cprinter.print_c();
    } else if (config.ast_dump) {
        Kolibri::AstPrinter printer;
        node->accept(printer);
    } else {
        return create_executable(cprinter, config);
    }


    return 0;
}
