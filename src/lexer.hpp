#pragma once

#include <string>

#include "token.hpp"

namespace Kolibri {

class Lexer {
public:
    Lexer(std::string source);
    Token get_next_token();
private:
    void skip_whitespace_and_comment();
    bool current_char_is_whitespace();
    bool current_char_is_comment();
    void advance();
    char peek(int i = 1);
    Token next_key_var();
    Token next_number();
    Token next_char();
    Token next_string();
    Token next_class_member();

    const std::string m_source;
    char m_current_char;
    int m_position;
    int m_line;
    int m_column;
    // ErrorClass
};

}
