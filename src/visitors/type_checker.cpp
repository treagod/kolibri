#include "type_checker.hpp"
#include "../parser.hpp"
#include "../util.hpp"

#include <utility>

namespace Kolibri {

class Scope {
public:
    Scope() {}
    Scope(Scope* parent) : m_parent_scope(parent) {}
    Scope* get_parent() { return m_parent_scope; }
    bool exists_in_current_scope(std::string var_name) {
        return m_variables.find(var_name) != m_variables.end();
    }
    bool exists(std::string var_name) {
        bool exists = m_variables.find(var_name) != m_variables.end();

        if (!exists) {
            if (m_parent_scope != nullptr) {
                return m_parent_scope->exists(var_name);
            } else {
                return false;
            }
        }

        return exists;
    }
    AstNode* get_variable(std::string var_name) {
        if (!exists_in_current_scope(var_name)) {
            return m_parent_scope->get_variable(var_name);
        }
        return m_variables.at(var_name).second;
    }
    std::string get_type(std::string var_name) {
        if (!exists_in_current_scope(var_name)) {
            return m_parent_scope->get_type(var_name);
        }
        return m_variables.at(var_name).first;
    }
    void define(std::string name, std::string type, AstNode* item) { m_variables[name] = {type, item};}
private:
    std::map<std::string, std::pair<std::string, AstNode*>> m_variables;
    Scope* m_parent_scope = nullptr;
};

/*
    ------------------- TYPE_CHECKER -------------------
*/

TypeChecker::TypeChecker(const FunctionTable& table, Module *main_module) {
    m_function_table = table;
    m_current_scope = new Scope();
    m_main_module = main_module;
    m_modules.push_back(main_module);
}

void TypeChecker::visit(BlockStmt* block) {
    for(auto child : block->get_children()) {
        child->accept(*this);
    }
}

void TypeChecker::visit(IfStmt* if_stmt) {
    m_current_scope = new Scope(m_current_scope);
    if_stmt->get_condition()->accept(*this);
    for(auto else_if : if_stmt->get_else_ifs()) {
        else_if->accept(*this);
    }
    for(auto child : if_stmt->get_children()) {
        child->accept(*this);
    }


    if(if_stmt->get_else_stmt() != nullptr) {
        if_stmt->get_else_stmt()->accept(*this);
    }

    auto scope = m_current_scope;
    m_current_scope = m_current_scope->get_parent();
    delete scope;
}

void TypeChecker::visit(BinaryExpr* bin_expr) {
    auto op_type = bin_expr->get_token().get_token_type();
    if (op_type == Equal || op_type == PlusEqual || op_type == MinusEqual
            || op_type == AsterixEqual || op_type == SlashEqual) {
        auto token = bin_expr->get_left_expr()->get_token();
        if (token.get_token_type() == Identifier) {
            if(bin_expr->get_left_expr()->is_mutable()) bin_expr->set_mutable();
            if(m_current_scope->exists(token.get_lexeme())) {
                auto variable_name = token.get_lexeme();
                auto variable = static_cast<Expr*>(m_current_scope->get_variable(variable_name));
                if (variable->is_mutable()) bin_expr->set_mutable();
                auto type = m_current_scope->get_type(variable_name);
                bin_expr->get_right_expr()->accept(*this);
                if (type != bin_expr->get_right_expr()->get_type()) {
                    std::cerr << "Variable " << variable_name << " is a ";
                    std::cerr << m_current_scope->get_type(variable_name);
                    std::cerr << ", but right side evaluates to ";
                    std::cerr << bin_expr->get_right_expr()->get_type();
                    std::cerr << ".\n";
                    exit(1);
                }

                if(!variable->is_mutable()) {
                    std::cerr << "Can't assign new value to immutable variable.\n";
                    std::cerr << token.get_line() << ":" << token.get_column() << "\n";
                    exit(1);
                }
            } else {
                std::cerr << "Unknown variable " << token.get_lexeme() << "\n";
                exit(1);
            }
        } else {
            std::cerr << "Left side is not assignable.\n";
            exit(1);
        }
    }
    bin_expr->get_left_expr()->accept(*this);
    bin_expr->get_right_expr()->accept(*this);
    auto type = bin_expr->get_token().get_token_type();
    if (boolean_operation(type)) {
        bin_expr->set_type("bool");
    } else {
        bin_expr->set_type(bin_expr->get_left_expr()->get_type());
    }
}

bool TypeChecker::boolean_operation(TokenType op) {
    switch (op) {
    case EqualEqual:
    case And:
    case Or:
    case Less:
    case LessEqual:
    case Greater:
    case GreaterEqual:
        return true;
    default:
        return false;
    }
}

void TypeChecker::visit(UnaryExpr* unary_expr) {
    unary_expr->set_type(unary_expr->get_expr()->get_type());
}


void TypeChecker::visit(IntLiteral*) {

}

void TypeChecker::visit(TrueLiteral*) {

}

void TypeChecker::visit(FalseLiteral*) {

}

void TypeChecker::visit(CharLiteral*) {

}

void TypeChecker::visit(IdentifierLiteral* ident_lit) {
    if (m_current_scope->exists(ident_lit->get_name())) {
        ident_lit->set_type(m_current_scope->get_type(ident_lit->get_name()));
    } else {
        std::cerr << "No variable " << ident_lit->get_name() << " defined. (";
        std::cerr << ident_lit->get_token().get_line() << ":" ;
        std::cerr << ident_lit->get_token().get_column() << ")\n";
        exit(1);
    }
}

void TypeChecker::visit(ForStmt* for_stmt) {
    m_current_scope = new Scope(m_current_scope);

    m_in_for_condition = true;
    for_stmt->get_condition()->accept(*this);
    m_in_for_condition = false;
    for(auto child :  for_stmt->get_children()) {
        child->accept(*this);
    }

    auto scope = m_current_scope;
    m_current_scope = m_current_scope->get_parent();
    delete scope;
}

void TypeChecker::visit(UnlessStmt* unless_stmt) {
    for(auto child : unless_stmt->get_children()) {
        child->accept(*this);
    }

    if(unless_stmt->get_else_stmt() != nullptr) {
        unless_stmt->get_else_stmt()->accept(*this);
    }
}

void TypeChecker::visit(ElseStmt* else_stmt) {
    m_current_scope = new Scope(m_current_scope);
    for(auto else_child : else_stmt->get_children()) {
        else_child->accept(*this);
    }
    auto scope = m_current_scope;
    m_current_scope = m_current_scope->get_parent();
    delete scope;
}

void TypeChecker::visit(LetStmt* let_stmt) {
    auto identifier = let_stmt->get_identifier();
    auto expr = let_stmt->get_assignment();
    if (m_current_scope->exists_in_current_scope(identifier->get_name())) {
        std::cerr << "Variable " << identifier->get_name() << " already defined.\n";
        exit(1);
    }
    expr->accept(*this);
    identifier->set_type(expr->get_type());
    identifier->set_mutable();
    expr->set_mutable();
    m_current_scope->define(identifier->get_name(), identifier->get_type(), expr);
}

void TypeChecker::visit(VarStmt* var_stmt) {
    auto identifier = var_stmt->get_identifier();
    auto expr = var_stmt->get_assignment();
    if (m_current_scope->exists_in_current_scope(identifier->get_name())) {
        std::cerr << "Variable " << identifier->get_name() << " already defined.\n";
        exit(1);
    }
    expr->accept(*this);
    identifier->set_type(expr->get_type());
    m_current_scope->define(identifier->get_name(), identifier->get_type(), identifier);
}

void TypeChecker::visit(ExprStmt* expr_stmt) {
    expr_stmt->get_expr()->accept(*this);
}

void TypeChecker::visit(FunctionDecl* func_decl) {
    m_current_scope = new Scope(m_current_scope);
    for (auto param : func_decl->get_params()) {
        m_current_scope->define(param->get_name(), param->get_type(), param);
    }
    for(auto child : func_decl->get_children()) {
        child->accept(*this);
    }
    auto tmp_scope = m_current_scope;
    m_current_scope = m_current_scope->get_parent();
    delete tmp_scope;
}

void TypeChecker::visit(FunctionCall* func_call) {
    auto call_name = func_call->get_name();
    if(call_name == "puts") {
        for (auto param : func_call->get_params()) {
            param->accept(*this);
        }
        if (func_call->get_params().size() != 1) {
            std::cerr << "Wrong amount of parameters. Expected " << 1;
            std::cerr<< " got " << func_call->get_params().size() << ".\n";
            exit(1);
        }
        auto param = func_call->get_params().at(0);
        if (param->get_type() != "string") {
            if(!m_function_table.exists(param->get_type() + "_to_s")) {
                std::cerr << "Can't convert " << param->get_type();
                std::cerr << " to string.\n";
                exit(1);
            }
        }
    } else {
        if(!m_function_table.exists(call_name)) {
            std::cerr << "No function " << call_name << " defined.\n";
            exit(1);
        }
        auto decl = m_function_table.get_function_decl(call_name);
        auto decl_params = decl->get_params();
        if (decl_params.size() != func_call->get_params().size()) {
            std::cerr << "Wrong amount of parameters. Expected " << decl_params.size();
            std::cerr<< " got " << func_call->get_params().size() << ".\n";
            exit(1);
        }

        for (auto param : func_call->get_params()) {
            param->accept(*this);
        }

        auto i1 = begin(decl_params);
        auto i2 = begin(func_call->get_params());
        for (auto e = end(decl_params); i1 != e; ++i1, ++i2) {
            if ((*i1)->get_type() != (*i2)->get_type()) {
                bool first = true;
                std::cerr << "Call parameters do not match any definition.\n";
                std::cerr << "Got:\n";
                std::cerr << "\t" << call_name << "(";
                for (auto param : func_call->get_params()) {
                    if (first) first = false;
                    else std::cerr << ", ";
                    std::cerr << param->get_type();
                }
                std::cerr << ")\n";
                first = true;
                std::cerr << "Expected:\n";
                std::cerr << "\t" << call_name << "(";
                for (auto param : decl->get_params()) {
                    if (first) first = false;
                    else std::cerr << ", ";
                    std::cerr << param->get_type();
                }
                std::cerr << ")\n";
                exit(1);
            }
        }

        func_call->set_type(decl->get_return_type());
    }
}

void TypeChecker::visit(ReturnStmt* return_stmt) {
    if (return_stmt->get_expr() != nullptr) {
        return_stmt->get_expr()->accept(*this);
    }
}

void TypeChecker::visit(ListLiteral* list_literal) {
    auto items = list_literal->get_items();
    if (items.size() > 0) {
        auto first_item = items.front();
        first_item->accept(*this);
        list_literal->set_item_type(first_item->get_type());

        for(auto item : items) {
            item->accept(*this);
            if (item->get_type() != list_literal->get_item_type()) {
                std::cerr << "List of type " << list_literal->get_item_type();
                std::cerr << " can't contain item of type " << item->get_type();
                std::cerr << ".\n";
                exit(1);
            }
        }
    }
}

void TypeChecker::visit(MemberExpr* member_expr) {
    auto container = member_expr->get_container();
    container->accept(*this);
    if (m_current_scope->exists(container->get_type())) {
        auto type = m_current_scope->get_type(container->get_type());
        if (type == "struct") {
            auto struct_decl = (StructDecl*) m_current_scope->get_variable(container->get_type());
            bool has_member = false;

            for(auto member : struct_decl->get_members()) {
                if (member_expr->get_member()->get_token().get_lexeme()  == member.second.get_lexeme()) {
                    member_expr->set_type(member.first.get_lexeme());
                    has_member = true;
                }
            }
            if (!has_member) {
                std::cerr << "No member " << member_expr->get_member()->get_token().get_lexeme();
                std::cerr << " defined for struct " << struct_decl->get_name() << ".\n";
            }
        }
    }
}

void TypeChecker::visit(StructDecl* struct_decl) {
    m_current_scope->define(struct_decl->get_name(), "struct", struct_decl);
    // Todo: check fo double definitons
}

void TypeChecker::visit(StructInstance* struct_instance) {
    if (m_current_scope->exists(struct_instance->get_name())) {
        if (m_current_scope->get_type(struct_instance->get_name()) != "struct") {
            std::cerr << struct_instance->get_name() << " is not a struct.\n";
            exit(1);
        }
    } else {
        std::cerr << "No struct " << struct_instance->get_name();
        std::cerr << " defined.\n";
        exit(1);
    }
    struct_instance->set_type(struct_instance->get_name());
}

void TypeChecker::visit(StringLiteral*) {
}

void TypeChecker::visit(ImportStmt* import_stmt) {
    auto import_path = import_stmt->get_import_path();
    std::string filepath = "";
    if (import_path[0] == '/') {
        // Absolute path
    } else if (import_path[0] == '.') {
        // Relative path
        if (import_path[1] == '.') {

        } else {
            import_path = import_path.substr(2);
        }
        auto current_filepath = import_stmt->get_filepath();
        auto pos = current_filepath.rfind('/');
        filepath = current_filepath.substr(0, pos) + '/' + import_path + ".koli";
    } else {
        // Module path
    }

    std::string imported_src = read_file(filepath);

    Parser parser(imported_src, filepath);

    auto node = parser.parse_source();

    FunctionTable func_table;
    node->accept(func_table);

    TypeChecker type_checker {func_table};
    node->accept(type_checker);


    if(import_stmt->get_inclusion().size() == 0) {
        // No symbols were imported explicitly
    } else {

    }
}

void TypeChecker::visit(RangeExpr* range_expr) {
    auto step = range_expr->get_step();
    step->accept(*this);
    if(step->get_type() != "int") {
        std::cerr << "Range step must be of type int.";
        std::cerr << " Type " << step->get_type() << " was given.\n";
        exit(1);
    }
}

void TypeChecker::visit(InExpr* in_expr) {
    in_expr->set_type("in");
    in_expr->get_right()->accept(*this);
    if (m_in_for_condition) {
        if (in_expr->get_right()->get_type() == "range") {
            auto range_expr = (RangeExpr*) in_expr->get_right();
            m_current_scope->define(in_expr->get_left()->get_token().get_lexeme()
                                    , "int", range_expr);
        }
    } else {

    }
}

}
