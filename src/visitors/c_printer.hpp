#include "../ast_node.hpp"

#include <sstream>
#include <stack>

namespace Kolibri {

class CPrinter : public Visitor {
public:
    void visit(BlockStmt* tud);
    void visit(IfStmt*);
    void visit(BinaryExpr*);
    void visit(UnaryExpr*);
    void visit(IntLiteral*);
    void visit(TrueLiteral*);
    void visit(FalseLiteral*);
    void visit(CharLiteral*);
    void visit(IdentifierLiteral*);
    void visit(ForStmt*);
    void visit(UnlessStmt*);
    void visit(ElseStmt*);
    void visit(LetStmt*);
    void visit(VarStmt*);
    void visit(ExprStmt*);
    void visit(FunctionDecl*);
    void visit(FunctionCall*);
    void visit(ReturnStmt*);
    void visit(ListLiteral*);
    void visit(MemberExpr*);
    void visit(StructDecl*);
    void visit(StructInstance*);
    void visit(StringLiteral*);
    void visit(ImportStmt*);
    void visit(RangeExpr*);
    void visit(InExpr*);
    void print_c();
    std::string emit_c();
private:
    std::stack<int> variable_ids;
    int temp_id = 0;
    void create_temp_variable();
    std::string get_last_temp_variable();
    std::stringstream sstream;
    void indent();
    void init_structs();
    void create_list_operations(std::string type);
    int m_indent_level = 0;
    std::string m_ident = "  ";
    bool m_in_for_condition = false;
};

}
