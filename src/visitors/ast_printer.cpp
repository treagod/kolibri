#include "ast_printer.hpp"

namespace Kolibri {

void AstPrinter::indent() {
    for(int i = 0; i < m_indent_level; i++) {
        std::cout << m_ident;
    }
}

void AstPrinter::visit(StructDecl*) {
}

void AstPrinter::visit(BlockStmt* tud) {
    indent();
    std::cout << "TranslationUnitDecl\n";
    m_indent_level++;
    for(const auto child : tud->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
}
void AstPrinter::visit(FunctionDecl* func_decl) {
    indent();
    m_indent_level++;
    for(auto child : func_decl->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
}
void AstPrinter::visit(IfStmt* if_stmt) {
    indent();
    std::cout << "IfStmt\n";
    m_indent_level++;
    for(const auto child : if_stmt->get_children()) {
        child->accept(*this);
    }
    for(const auto else_if : if_stmt->get_else_ifs()) {
        else_if->accept(*this);
    }
    auto else_stmt = if_stmt->get_else_stmt();

    if (else_stmt != nullptr) {
        else_stmt->accept(*this);
    }

    m_indent_level--;
}

void AstPrinter::visit(BinaryExpr* bin_expr) {
    indent();
    std::cout << "BinaryExpr value='";
    std::cout << bin_expr->get_token().get_lexeme();
    std::cout << "'\n";
    m_indent_level++;
    bin_expr->get_left_expr()->accept(*this);
    bin_expr->get_right_expr()->accept(*this);
    m_indent_level--;
}

void AstPrinter::visit(UnaryExpr* bin_expr) {
}

void AstPrinter::visit(IntLiteral* int_literal) {
    indent();
    std::cout << "IntLiteral value='";
    std::cout << int_literal->get_token().get_lexeme();
    std::cout << "'\n";
}
void AstPrinter::visit(TrueLiteral* true_literal) {
    indent();
    std::cout << "TrueLiteral value='";
    std::cout << true_literal->get_token().get_lexeme();
    std::cout << "'\n";
}
void AstPrinter::visit(FalseLiteral* false_literal) {
    indent();
    std::cout << "FalseLiteral value='";
    std::cout << false_literal->get_token().get_lexeme();
    std::cout << "'\n";
}
void AstPrinter::visit(CharLiteral* char_literal) {
    indent();
    std::cout << "CharLiteral value='";
    std::cout << char_literal->get_token().get_lexeme();
    std::cout << "'\n";
}
void AstPrinter::visit(IdentifierLiteral* ident_literal) {
    indent();
    std::cout << "IdentifierLiteral name='";
    std::cout << ident_literal->get_token().get_lexeme();
    std::cout << "'\n";
}
void AstPrinter::visit(ForStmt* for_stmt) {
    std::cout << "ForStmt\n";
    m_indent_level++;
    for_stmt->get_condition()->accept(*this);
    m_indent_level--;
}

void AstPrinter::visit(UnlessStmt* unless_stmt) {
    indent();
    std::cout << "UnlessStmt\n";
    m_indent_level++;
    for(const auto child : unless_stmt->get_children()) {
        child->accept(*this);
    }

    auto else_stmt = unless_stmt->get_else_stmt();

    if (else_stmt != nullptr) {
        else_stmt->accept(*this);
    }

    m_indent_level--;
}
void AstPrinter::visit(ElseStmt* else_stmt) {
    indent();
    std::cout << "ElseStmt\n";
    m_indent_level++;
    for(auto child : else_stmt->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
}
void AstPrinter::visit(LetStmt* let_stmt) {
    indent();
    std::cout << "LetStmt\n";
    m_indent_level++;
    let_stmt->get_assignment()->accept(*this);
    m_indent_level--;
}
void AstPrinter::visit(VarStmt* var_stmt) {
    indent();
    std::cout << "VarStmt\n";
    m_indent_level++;
    var_stmt->get_assignment()->accept(*this);
    m_indent_level--;
}

void AstPrinter::visit(ExprStmt*) {

}

void AstPrinter::visit(RangeExpr*) {

}

void AstPrinter::visit(FunctionCall* function_call) {

}

void AstPrinter::visit(ReturnStmt* return_stmt) {

}

void AstPrinter::visit(ListLiteral* list_literal) {
}

void AstPrinter::visit(MemberExpr* member_expr) {
}

void AstPrinter::visit(StructInstance*) {
}

void AstPrinter::visit(StringLiteral*) {
}

void AstPrinter::visit(ImportStmt*) {
}

void AstPrinter::visit(InExpr* in_expr) {
    indent();
    std::cout << "InExpr iterate_var='";
    in_expr->get_left()->accept(*this);
    std::cout << "' start=";
    in_expr->get_right()->accept(*this);
    std::cout << "\n";
}

}
