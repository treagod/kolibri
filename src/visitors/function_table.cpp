#include "function_table.hpp"

namespace Kolibri {

void FunctionTable::visit(BlockStmt* tud) {
    for(auto child : tud->get_children()) {
        child->accept(*this);
    }
}

void FunctionTable::visit(LetStmt* let_stmt) {}

void FunctionTable::visit(VarStmt* var_stmt) {}

void FunctionTable::visit(FunctionDecl* func_decl) {
    if (func_decl->get_name() == "main") {
        if (m_has_main) {
            std::cerr << "More than one main function defined.\n";
            exit(1);
        }
        m_has_main = true;
    }
    add_function(func_decl);
}

void FunctionTable::add_function(FunctionDecl* function_decl) {
    if(m_functions.find(function_decl->get_name()) != m_functions.end()) {
        std::cerr << "Function " << function_decl->get_name() << " already defined in ";
        std::cerr << "\"" << function_decl->get_filepath() << "\" at " << function_decl->get_token().get_line();
        std::cerr << ":" << function_decl->get_token().get_column() << ".\n";
        exit(1);
    }
    m_functions[function_decl->get_name()] = function_decl;
}

void FunctionTable::visit(IfStmt*) {

}
void FunctionTable::visit(BinaryExpr*) {
}

void FunctionTable::visit(UnaryExpr* bin_expr) {
}


void FunctionTable::visit(IntLiteral*) {

}
void FunctionTable::visit(TrueLiteral*) {

}
void FunctionTable::visit(FalseLiteral*) {

}
void FunctionTable::visit(CharLiteral*) {

}
void FunctionTable::visit(IdentifierLiteral*) {

}
void FunctionTable::visit(ForStmt*) {

}
void FunctionTable::visit(UnlessStmt*) {

}
void FunctionTable::visit(ElseStmt*) {

}
void FunctionTable::visit(ExprStmt*) {

}

void FunctionTable::visit(FunctionCall* func_call) {
}

void FunctionTable::visit(ReturnStmt* return_stmt) {

}

void FunctionTable::visit(ListLiteral* list_literal) {
}

void FunctionTable::visit(MemberExpr* member_expr) {
}

void FunctionTable::visit(StructDecl*) {
}

void FunctionTable::visit(StructInstance*) {
}

void FunctionTable::visit(StringLiteral*) {
}

void FunctionTable::visit(ImportStmt*) {
}

void FunctionTable::visit(RangeExpr*) {

}

void FunctionTable::visit(InExpr*) {
}

}
