#include "../ast_node.hpp"

#include <string>

namespace Kolibri {

class AstPrinter : public Visitor {
public:
    void visit(BlockStmt* tud);
    void visit(IfStmt*);
    void visit(BinaryExpr*);
    void visit(UnaryExpr*);
    void visit(IntLiteral*);
    void visit(TrueLiteral*);
    void visit(FalseLiteral*);
    void visit(CharLiteral*);
    void visit(IdentifierLiteral*);
    void visit(ForStmt*);
    void visit(UnlessStmt*);
    void visit(ElseStmt*);
    void visit(LetStmt*);
    void visit(VarStmt*);
    void visit(ExprStmt*);
    void visit(FunctionDecl*);
    void visit(ReturnStmt*);
    void visit(FunctionCall*);
    void visit(ListLiteral*);
    void visit(MemberExpr*);
    void visit(StructDecl*);
    void visit(StructInstance*);
    void visit(StringLiteral*);
    void visit(ImportStmt*);
    void visit(RangeExpr*);
    void visit(InExpr*);
private:
    void indent();
    int m_indent_level = 0;
    std::string m_ident = "  ";
};

}
