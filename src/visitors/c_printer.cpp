#include "c_printer.hpp"

namespace Kolibri {
static const std::string tmp_name = "_temp_kolibri_";
static const std::string li_name = "__kolibri_list";

std::string get_c_name(const std::string& var) {
    if (var == "string")  {
        return "_kolibri_string";
    } else if (var == "bool") {
        return "char";
    }
    return var;
}

void CPrinter::init_structs() {
    sstream << "typedef struct {\n";
    m_indent_level++;
    indent(); sstream << "int capacity;\n";
    indent(); sstream << "int size;\n";
    indent(); sstream << "void* data;\n";
    m_indent_level--;
    sstream << "} " << li_name << ";\n\n";
}

void CPrinter::visit(StructDecl* struct_decl) {
    sstream << "typedef struct _" << struct_decl->get_name() << "{\n";
    m_indent_level++;
    std::vector<std::string> member_names;
    for(auto member : struct_decl->get_members()) {

    }
    for(auto member : struct_decl->get_members()) {
        indent();
        sstream << get_c_name(member.first.get_lexeme()) << " " << member.second.get_lexeme() << ";\n";
    }
    m_indent_level--;
    sstream << "} " << struct_decl->get_name() << ";\n\n";
}

void CPrinter::visit(StructInstance* struct_instance) {
    bool first = true;
    std::vector<std::string> member_temp_names;
    for (auto member : struct_instance->get_members()) {
        member.second->accept(*this);
        member_temp_names.push_back(get_last_temp_variable());
    }
    sstream << " { ";
    for (unsigned long i = 0; i < struct_instance->get_members().size(); i++) {
        auto member = struct_instance->get_members().at(i);
        auto member_temp_name = member_temp_names.at(i);
        if (first) first = false;
        else sstream << ", ";
        sstream << "." <<   member.first.get_lexeme() << " = ";
        sstream << member_temp_name << ";\n";
    }

    sstream << " } ";
}

void CPrinter::indent() {
    for(int i = 0; i < m_indent_level; i++) {
        sstream << m_ident;
    }
}

void CPrinter::visit(BlockStmt* tud) {
    m_indent_level++;
    for(auto child : tud->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
}

void CPrinter::visit(FunctionDecl* func_decl) {
    bool first = true;
    auto return_type =  func_decl->get_return_type() == "bool" ? "char" :  func_decl->get_return_type();
    // sstream << "#line ";
    // sstream << func_decl->get_token().get_line();
    // sstream << " \"" << func_decl->get_filepath() << "\"\n";
    sstream << return_type << " " << func_decl->get_name();
    sstream << "(";
    for(auto param : func_decl->get_params()) {
        if(first) first = false;
        else sstream << ", ";
        if(!param->is_mutable()) sstream << "const ";

        sstream << param->get_type() << " " << param->get_token().get_lexeme();
    }
    sstream << ") {\n";
    m_indent_level++;
    for(auto child : func_decl->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
    sstream << "}\n";
}

void CPrinter::visit(IfStmt* if_stmt) {
    // sstream << "#line ";
    // sstream << if_stmt->get_token().get_line();
    // sstream << " \"" << if_stmt->get_filepath() << "\"\n";
    if_stmt->get_condition()->accept(*this);
    auto if_cond = get_last_temp_variable();
    std::vector<std::pair<std::string, IfStmt*>> elsif_conds;

    for(auto elsif : if_stmt->get_else_ifs()) {
        elsif->get_condition()->accept(*this);
        elsif_conds.push_back(std::make_pair(get_last_temp_variable(), elsif));
    }
    indent();
    sstream << "if ( ";
    sstream << if_cond;
    sstream << " ) {\n";
    m_indent_level++;
    for(auto child : if_stmt->get_children()) {
        child->accept(*this);
    }
    for(auto elsif : elsif_conds) {
        m_indent_level--;
        indent();
        sstream << "} ";
        sstream << "else if ( ";
        sstream << elsif.first;
        sstream << " ) {\n";
        m_indent_level++;
        for(auto child : elsif.second->get_children()) {
            child->accept(*this);
        }
    }
    m_indent_level--;
    indent();
    sstream << "}\n";
    if (if_stmt->get_else_stmt() != nullptr) {
        if_stmt->get_else_stmt()->accept(*this);
    }
}

void CPrinter::visit(BinaryExpr* binary_expr) {
    auto left_expr = binary_expr->get_left_expr();
    auto right_expr = binary_expr->get_right_expr();
    binary_expr->get_left_expr()->accept(*this);
    binary_expr->get_right_expr()->accept(*this);

    indent();

    auto op_type = binary_expr->get_token().get_token_type();
    if (op_type == Equal || op_type == PlusEqual || op_type == MinusEqual
            || op_type == AsterixEqual || op_type == SlashEqual) {
        sstream << binary_expr->get_left_expr()->get_token().get_lexeme() << " = ";
    } else {
        sstream << get_c_name(binary_expr->get_type());
        sstream << " " << tmp_name << temp_id << " = ";
    }

    int right = variable_ids.top(); variable_ids.pop();
    int left  = variable_ids.top(); variable_ids.pop();
    variable_ids.push(temp_id++);
    if (left_expr->get_type() == "string") {
        std::string type = get_c_name( right_expr->get_type() );

        sstream << "_kolibri_string_plus_" << type << "( ";
        sstream << "&" << " _temp_kolibri_" << left;
        sstream << ", ";
        if (!(right_expr->get_type() == "char" || right_expr->get_type() == "int")) {
            sstream << "& ";
        }
        sstream << "_temp_kolibri_" << right;
        sstream << " )";

    } else {
        sstream << tmp_name << left;
        sstream << binary_expr->get_token().get_lexeme();
        sstream << tmp_name << right;
    }
    sstream << ";\n";
}

void CPrinter::visit(UnaryExpr* unary_expr) {
    sstream << unary_expr->get_token().get_lexeme();
    unary_expr->get_expr()->accept(*this);
}


void CPrinter::visit(IntLiteral* int_literal) {
    indent();
    sstream << "int ";
    create_temp_variable();
    sstream << int_literal->get_token().get_lexeme();
    sstream << ";\n";
}

void CPrinter::visit(TrueLiteral*) {
    indent();
    sstream << "char ";
    create_temp_variable();
    sstream << "1;\n";
}

void CPrinter::visit(FalseLiteral*) {
    indent();
    sstream << "char ";
    create_temp_variable();
    sstream << "0;\n";
}

void CPrinter::visit(CharLiteral* char_literal) {
    indent();
    sstream << "char ";
    create_temp_variable();
    sstream << '\'' << char_literal->get_token().get_lexeme() << '\'';
    sstream << ";\n";
}

void CPrinter::visit(IdentifierLiteral* ident_literal) {
    indent();
    sstream << get_c_name(ident_literal->get_type()) << " ";
    create_temp_variable();
    sstream << ident_literal->get_name();
    sstream << ";\n";
}



void CPrinter::visit(StringLiteral* str_literal) {
    indent();
    sstream << get_c_name("string") << " ";
    create_temp_variable();
    sstream << " create_kolibri_string( \"";
    sstream << str_literal->get_token().get_lexeme();
    sstream << "\" );\n";
}


void CPrinter::visit(ForStmt* for_stmt) {
    for_stmt->get_condition()->accept(*this);

    m_in_for_condition = true;
    auto condition = for_stmt->get_condition();
    std::string tmp_var = get_last_temp_variable();
    if(condition->get_type() == "bool") {
        indent(); sstream << "for(";
        sstream << ";";
        sstream << tmp_var;
        sstream << ";";
    } else {
        if(condition->get_type() == "in") {
            auto in_expr = static_cast<InExpr*>(condition);
            if(in_expr->get_right()->get_type() == "range") {
                auto range_expr = static_cast<RangeExpr*>(in_expr->get_right());
                range_expr->get_lower()->accept(*this);
                range_expr->get_upper()->accept(*this);
                range_expr->get_step()->accept(*this);
                std::string step = get_last_temp_variable();
                std::string upper = get_last_temp_variable();
                std::string lower = get_last_temp_variable();
                std::string iter_var = in_expr->get_left()->get_token().get_lexeme();
                indent(); sstream << "for(";
                sstream << "int " << iter_var;
                sstream << "= " << lower;
                sstream << "; " << iter_var;
                sstream << " < " << upper;
                sstream << "; " << iter_var;
                sstream << " += " << step;
            }
        }
        //for_stmt->get_condition()->accept(*this);
    }
    m_in_for_condition = false;
    sstream << ") {\n";
    m_indent_level++;
    for(auto child : for_stmt->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
    for_stmt->get_condition()->accept(*this);
    if(condition->get_type() == "bool") {
        indent();
        sstream << tmp_var << " = ";
        sstream << get_last_temp_variable() << ";\n";
    }
    indent(); sstream << "}\n";
}

void CPrinter::visit(UnlessStmt* unless_stmt) {
    // sstream << "#line ";
    // sstream << unless_stmt->get_token().get_line();
    // sstream << " \"" << unless_stmt->get_filepath() << "\"\n";
    unless_stmt->get_condition()->accept(*this);
    indent();
    sstream << "if ( !(";
    sstream << get_last_temp_variable();
    sstream << ") ) {\n";
    m_indent_level++;
    for(auto child : unless_stmt->get_children()) {
        child->accept(*this);
    }

    m_indent_level--;
    indent();
    sstream << "}\n";
    unless_stmt->get_else_stmt()->accept(*this);
}

void CPrinter::visit(ElseStmt* else_stmt) {
    // sstream << "#line ";
    // sstream << else_stmt->get_token().get_line();
    // sstream << " \"" << else_stmt->get_filepath() << "\"\n";
    indent();
    sstream << "else {\n";
    m_indent_level++;
    for(auto child : else_stmt->get_children()) {
        child->accept(*this);
    }
    m_indent_level--;
    indent();
    sstream << "}\n";
}

void CPrinter::visit(LetStmt* let_stmt) {
    // sstream << "#line ";
    // sstream << let_stmt->get_token().get_line();
    // sstream << " \"" << let_stmt->get_filepath() << "\"\n";
    let_stmt->get_assignment()->accept(*this);
    indent();

    std::string type_name = let_stmt->get_identifier()->get_type();
    if (let_stmt->get_assignment()->get_type() == "list") {
        sstream << "const " << li_name << " ";
    } else {
        sstream << "const " << get_c_name(type_name) << " ";
    }
    sstream << let_stmt->get_identifier()->get_token().get_lexeme() << " = ";

    sstream << get_last_temp_variable();
    sstream << ";\n";
}

void CPrinter::visit(VarStmt* var_stmt) {
    // sstream << "#line ";
    // sstream << var_stmt->get_token().get_line();
    // sstream << " \"" << var_stmt->get_filepath() << "\"\n";
    var_stmt->get_assignment()->accept(*this);
    indent();

    // If the found type is a bool, create a char type in C Target
    std::string type_name = var_stmt->get_identifier()->get_type();
    if (var_stmt->get_assignment()->get_type() == "list") {
        sstream << li_name << " ";
    } else {
        sstream << get_c_name(type_name) << " ";
    }
    sstream << var_stmt->get_identifier()->get_token().get_lexeme() << " = ";

    sstream << get_last_temp_variable();
    sstream << ";\n";
}

void CPrinter::visit(ExprStmt* expr_stmt) {
    // sstream << "#line ";
    // sstream << expr_stmt->get_token().get_line();
    // sstream << " \"" << expr_stmt->get_filepath() << "\"\n";
    expr_stmt->get_expr()->accept(*this);
    //sstream << ";\n";
}

void CPrinter::visit(FunctionCall* func_call) {
    std::string name = func_call->get_name();
    std::vector<std::pair<std::string, std::string>> params;
    for (auto param : func_call->get_params()) {
        param->accept(*this);
        params.push_back(std::make_pair(tmp_name + std::to_string(variable_ids.top()), param->get_type()));
        variable_ids.pop();
    }

    if(name == "puts") {
        bool first = true;

        auto param = params.at(0);
        std::string output;

        if (param.second == "string") {
            output = param.first;
        } else {
            indent();
            sstream << get_c_name("string") << " ";
            create_temp_variable();
            sstream << param.second << "_to_s(";
            if(!(param.second == "char" || param.second == "bool"
                 || param.second == "int")) {
                sstream << "&";
            }
            sstream << param.first << ");\n";
            output = get_last_temp_variable();
        }


        indent();
        sstream << "puts_kolibri(&";
        sstream << output;
        sstream << ")";
    } else {
        bool first = true;
        indent();

        if (func_call->get_type() != "void") {
            sstream << get_c_name(func_call->get_type()) << " ";
            create_temp_variable();
        }
        sstream << func_call->get_name();
        sstream << "( ";
        for (auto param : params) {
            if (!first) {
                sstream << ", ";
            } else {
                first = false;
            }
            if (!(param.second == "int" || param.second == "char")) {
                sstream << "& ";
            }

            sstream << param.first;
        }
        sstream << " )";
    }

    sstream << ";\n";
}

void CPrinter::visit(ReturnStmt* return_stmt) {
    // sstream << "#line ";
    // sstream << return_stmt->get_token().get_line();
    // sstream << " \"" << return_stmt->get_filepath() << "\"\n";
    return_stmt->get_expr()->accept(*this);
    indent();
    sstream << return_stmt->get_token().get_lexeme();
    sstream << " ";
    if (return_stmt->get_expr() != nullptr) {
        sstream << get_last_temp_variable();
    }
    sstream << ";\n";
}

void CPrinter::visit(ListLiteral* list_literal) {
    sstream << " create_" << li_name << "_" << list_literal->get_item_type() << "()";
}

void CPrinter::visit(MemberExpr* member_expr) {
    if (member_expr->get_container()->get_type() == "list") {
        auto list = (ListLiteral*) member_expr->get_container();
        // Todo simpler access
        if (member_expr->get_member()->get_token().get_lexeme() == "append") {
            // Todo: logic
            sstream << "int _list_kolibri_temp = 6;\n";
            // insert_int_into___kolibri_list(__kolibri_list* list, void* new_data)
            sstream << "insert_int_into_";
            sstream << li_name << "(&" << list->get_token().get_lexeme();
            sstream << ", &_list_kolibri_temp)";
        }
    } else {
        sstream << member_expr->get_container()->get_token().get_lexeme() << ".";
        sstream << member_expr->get_member()->get_token().get_lexeme();
    }
}

void CPrinter::visit(ImportStmt*) {
}

void CPrinter::visit(RangeExpr*) {

}

void CPrinter::visit(InExpr* in_expr) {
    if (m_in_for_condition) {
        if (in_expr->get_right()->get_type() == "range") {
            auto range_expr = (RangeExpr*) in_expr->get_right();
            sstream << "int ";
            in_expr->get_left()->accept(*this);
            sstream << " = ";
            range_expr->get_lower()->accept(*this);
            sstream << ";";
            in_expr->get_left()->accept(*this);
            sstream << " < ";
            range_expr->get_upper()->accept(*this);
            sstream << ";";
            in_expr->get_left()->accept(*this);
            sstream << " += ";
            range_expr->get_step()->accept(*this);
        } else {

        }
    } else {

    }
}


void CPrinter::create_temp_variable() {
    sstream << "_temp_kolibri_" << temp_id << " = ";
    variable_ids.push(temp_id++);
}

std::string CPrinter::get_last_temp_variable() {
    std::string s =  "_temp_kolibri_" + std::to_string(variable_ids.top());
    variable_ids.pop();
    return s;
}

void CPrinter::print_c() {
    std::cout << "#include <stdio.h>\n";
    std::cout << "#include <stdlib.h>\n";
    std::cout << "#include \"/home/mahlgrimm/Work/CPP/kolibri/kolibbri_libs/kolibri_runtime.h\"\n\n";
    std::cout << sstream.str();
}

std::string CPrinter::emit_c() {
    std::stringstream output;
    output << "#include <stdio.h>\n";
    output << "#include <stdlib.h>\n";
    output << "#include \"/home/mahlgrimm/Work/CPP/kolibri/kolibbri_libs/kolibri_runtime.h\"\n\n";
    output << sstream.str();
    return output.str();
}

}
