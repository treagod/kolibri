#pragma once

#include "../ast_node.hpp"

#include <map>

namespace Kolibri {

class FunctionTable : public Visitor {
public:
    FunctionTable() {
        auto puts_decl = new FunctionDecl(Token("puts", 0, 0, 0, Identifier), "");
        auto puts_param = new IdentifierLiteral(Token("value", -1, -1, -1, Identifier), "");
        puts_param->set_type("string");
        puts_decl->add_param(puts_param);
        m_functions["puts"] = puts_decl;
        auto int_to_s_decl = new FunctionDecl(Token("int_to_s", 0, 0, 0, Identifier), "");
        auto int_to_s_param = new IdentifierLiteral(Token("value", 0, 0, 0, Identifier), "");
        int_to_s_param->set_type("int");
        int_to_s_decl->add_param(int_to_s_param);
        m_functions["int_to_s"] = int_to_s_decl;
    }
    void visit(BlockStmt* tud);
    void visit(IfStmt*);
    void visit(BinaryExpr*);
    void visit(UnaryExpr*);
    void visit(IntLiteral*);
    void visit(TrueLiteral*);
    void visit(FalseLiteral*);
    void visit(CharLiteral*);
    void visit(IdentifierLiteral*);
    void visit(ForStmt*);
    void visit(UnlessStmt*);
    void visit(ElseStmt*);
    void visit(LetStmt*);
    void visit(VarStmt*);
    void visit(ExprStmt*);
    void visit(FunctionDecl*);
    void visit(FunctionCall*);
    void visit(ReturnStmt*);
    void visit(ListLiteral*);
    void visit(MemberExpr*);
    void visit(StructDecl*);
    void visit(StructInstance*);
    void visit(StringLiteral*);
    void visit(ImportStmt*);
    void visit(RangeExpr*);
    void visit(InExpr*);
    bool has_main() { return m_has_main; }
    const std::map<std::string, FunctionDecl*>& get_functions() { return m_functions; }
    bool exists(std::string function_name) { return m_functions.find(function_name) != m_functions.end(); }
    FunctionDecl* get_function_decl(const std::string& function_name) const { return m_functions.at(function_name); }
private:
    void add_function(FunctionDecl* function_decl);
    AstNode* m_node;
    std::map<std::string, FunctionDecl*> m_functions;
    bool m_has_main = false;
};

}
