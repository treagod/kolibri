#pragma once

#include "../ast_node.hpp"
#include "../module.hpp"
#include "function_table.hpp"
#include "../module.hpp"

#include <map>

namespace Kolibri {

class Scope;
class TypeChecker : public Visitor {
public:
    TypeChecker(const FunctionTable& table, Module* main_module);
    void visit(BlockStmt* tud);
    void visit(IfStmt*);
    void visit(BinaryExpr*);
    void visit(UnaryExpr*);
    void visit(IntLiteral*);
    void visit(TrueLiteral*);
    void visit(FalseLiteral*);
    void visit(CharLiteral*);
    void visit(IdentifierLiteral*);
    void visit(ForStmt*);
    void visit(UnlessStmt*);
    void visit(ElseStmt*);
    void visit(LetStmt*);
    void visit(VarStmt*);
    void visit(ExprStmt*);
    void visit(FunctionDecl*);
    void visit(FunctionCall*);
    void visit(ReturnStmt*);
    void visit(ListLiteral*);
    void visit(MemberExpr*);
    void visit(StructDecl*);
    void visit(StructInstance*);
    void visit(StringLiteral*);
    void visit(ImportStmt*);
    void visit(RangeExpr*);
    void visit(InExpr*);
    Module get_as_module();
private:
    bool boolean_operation(TokenType op);
    FunctionTable m_function_table;
    Scope* m_current_scope;
    bool m_in_for_condition = false;
    Module* m_main_module;
    std::vector<Module*> m_modules;
};

}
