#include <string>

namespace Kolibri {

enum SymbolType {
    Struct, Var, Builtin
};

class Symbol {
public:
    Symbol(std::string name, SymbolType type);
    virtual ~Symbol();
    const std::string& get_name();
    SymbolType get_type();

private:
    std::string m_name;
    SymbolType m_type;
};

class SymbolFactory {
public:
    static Symbol new_struct_symbol(std::string name);
    static Symbol new_var_symbol(std::string name);
    static Symbol new_builtin_symbol(std::string name);
};

}
