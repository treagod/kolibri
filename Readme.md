# Kolibri
**Kolibri** is a lightweight typed language. It was only developed for educational purpose.

## Todo

- [ ] Templates (needed for Lists, Arrays, Set, Map)
- [ ] Lists
- [ ] Array
- [ ] Set
- [ ] Map
- [x] Functions
- [ ] Mutable Parameters
- [x] String
- [x] Structs
- [ ] Structs methods
- [ ] Structs operators
- [ ] Classes
- [ ] Imports
- [x] Ranges
- [ ] C Bindings
- [ ] Stdio (depending on C Bindings)

## Basic Syntax
```ruby
class Foo
  setter baz
  getter bar, baz

  init(string @bar, int baz)
    @baz = baz
  end
end

fn main(string[] args)
  let foo = new Foo("bar", 9001)

  puts("$(foo.bar) $(foo.baz)")
end
```

## Data Types
```ruby
let s = "Hello there, I'm a string"
let c = 'c' # ASCII character
let i = 1 # Integer
let u = 1u # Unsigned Integer
let l = 1l # Long
let f = 1.0 # Float
let a = [1, 2, 3] # Array
let l = (1, 2, 3) # List
let s = {1, 2, 3} # Set
let m = {1: 2, 3: 4} # Map
```

## Basic Statements
**Loops**
```ruby
for true
  puts("It goes on forever, forever..")
end

for i in 1..10
  puts(i)
end
```
**Conditions**
```ruby
if true
  puts("It's true. All of it")
elsif false
  puts("How did you get here?")
end

unless true
  puts("Not executed")
end

```
## Other stuff
In Kolibri null is no standard value. You have to use the **?=** operator to mark a variable as nullable.
A variable which is nullable has to be checked at each occurence for null.

```ruby
let animal ?= create_animal()

animal.make_noise() # Compiler error. Must be checked for null

if animal
  animal.make_noise() # Ok
else
  puts("There's nobody in the jungle..")
end
```